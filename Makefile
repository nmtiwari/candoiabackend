HADOOP_HOME=/home/hadoop/hadoop-current
HBASE_HOME=/home/hadoop/hbase-current
NATIVE_LIBS=${HADOOP_HOME}/lib/native/Linux-amd64-64/
MAXMEM=3072m

-include Makefile.local

CLASSPATH=lib/*:${HADOOP_HOME}/lib/*
JAVAC=javac -cp .:$(CLASSPATH)
JAVA=java -Xmx$(MAXMEM) -Djava.library.path=$(NATIVE_LIBS) -cp .:$(CLASSPATH)
JAVA_HBASE=$(JAVA) -XX:+UseConcMarkSweepGC

compile-all:
	$(JAVAC) main/*.java

compile-jgf:
	$(JAVAC) util/jgf/*.java

compile-issues:
	$(JAVAC) main/ParseSourceforgeIssues.java

run-issues:
	$(JAVA) main.ParseSourceforgeIssues /hadoop/sfnet/bugs/302891/1276944/index~1.html_group_id\=302891\&atid\=1276944\&func\=detail\&aid\=3549378

compile-seq:
	$(JAVAC) main/SeqImporter.java

run-seq:
	$(JAVA) main.SeqImporter

compile-hbase-comment-import:
	$(JAVAC) main/HBaseCommentImporter.java

run-hbase-comment-import:
	$(JAVA_HBASE) main.HBaseCommentImporter 2>> comment-import.err | tee -a comment-import.out

compile-hbase-repo-import:
	$(JAVAC) main/HBaseRepoImporter.java

run-hbase-repo-import:
	$(JAVA_HBASE) main.HBaseRepoImporter 2>> repo-import.err | tee -a repo-import.out

run-seq-repo-import:
	$(JAVA) main.SeqRepoImporter 2>> repo-import.err | tee -a repo-import.out

run-seq-proj-combine:
	$(JAVA) main.SeqProjectCombine 2>> proj-combine.err | tee -a proj-combine.out

run-mapfile-gen:
	$(JAVA) main.MapFileGen

run-seq-sort-test:
	$(JAVA) main.TestSorting 2>> repo-sort-test.err | tee -a repo-sort-test.out

run-seq-test:
	$(JAVA) main.TestSequenceFile

compile-hbase-proj-import:
	$(JAVAC) main/HBaseProjectImporter.java

run-hbase-proj-import:
	$(JAVA_HBASE) main.HBaseProjectImporter 2>> proj-import.err | tee -a proj-import.out

run-hbase-test-hoan:
	$(JAVA_HBASE) main.TestHBase 2>> test-hoan.err | tee -a test-hoan.out

run-test-hoan:
	$(JAVA) main.TestGitConnector

compile-seq-generator:
	$(JAVAC) main/SeqGenerator.java

run-seq-generator:
	$(JAVA) main.SeqGenerator -p -a -c 2>> generator.err | tee -a generator.out

run-seq-generator-projects:
	$(JAVA) main.SeqGenerator -p 2>> generator.err | tee -a generator.out

run-seq-generator-small:
	$(JAVA) main.SeqGenerator -s 2>> generator.err | tee -a generator.out

run-seq-generator-medium:
	$(JAVA) main.SeqGenerator -s -k 10 2>> generator.err | tee -a generator.out

run-seq-generator-ast:
	$(JAVA) main.SeqGenerator -a 2>> generator.err | tee -a generator.out

run-seq-generator-comments:
	$(JAVA) main.SeqGenerator -c 2>> generator.err | tee -a generator.out

compile-dumper:
	$(JAVAC) main/HBaseDumper.java

run-dumper:
	$(JAVA) main.HBaseDumper

compile-hbase:
	$(JAVAC) main/HBaseImporter.java

run-hbase:
	$(JAVA_HBASE) main.HBaseImporter 2>> import.err | tee -a import.out

compile-loc:
	$(JAVAC) main/HBaseLOC.java

run-loc:
	$(JAVA_HBASE) main.HBaseLOC 2>> locimport.err | tee -a locimport.out

compile-counter:
	$(JAVAC) main/HBaseCounter.java

run-counter:
	$(JAVA) main.HBaseCounter

compile-create-hbase:
	$(JAVAC) main/CreateTable.java

run-create-hbase:
	$(JAVA) main.CreateTable

compile-cache-json:
	$(JAVAC) main/CacheJSON.java

run-cache-json:
	$(JAVA) main.CacheJSON

verify-import:
	grep -v '^Accepted ' import.err |grep -v '^using '|grep -vi zookeeper|grep -v '^Found '

clean:
	find . -name '*.class' -exec rm '{}' ';'
