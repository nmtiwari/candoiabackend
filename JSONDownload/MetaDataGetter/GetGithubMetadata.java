package JSONDownload.MetaDataGetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import br.ufpe.cin.groundhog.http.HttpModule;

import com.google.inject.Guice;

import JSONDownload.github.MetadataCacher;
import JSONDownload.util.Config;
import JSONDownload.util.*;


public class GetGithubMetadata {
	
	
	public static void main(String []args) throws IOException{
		getMetaData(args);
	}
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	//username,password,usernameToBecloned,project,path
	public static void getMetaData(String[] args) throws IOException {
		String outDir ="";
		String username = Config.githubUsername;
		String password = Config.githubPassword;
		TokenGenerator builder = null;
		if (args != null && args.length > 0) {
			if (args.length >= 3) {
//						 username = args[0];
//						 password = args[1];
//						 outDir=args[4]+"/JSONData";
					 	outDir=args[2]+"/JSONData";
			}else{
				System.out.println("Arguments mismatch");
				return;
			}
		}
		//String url = "https://api.github.com/repos/"+args[2]+"/"+args[3];
		
		builder = Guice.createInjector(new HttpModule()).getInstance(TokenGenerator.class);
		String url = builder.uses("https://api.github.com/")
				  .withParam("repos")
				  .withSimpleParam("/", args[0])
				  .withSimpleParam("/", args[1])
				  .build();
				
		
		
		
		String pageContent = "";
		MetadataCacher mc = new MetadataCacher(url, username, password);
		int pageNumber = 1;
		if(args[1].equals("*")){
			url = "https://api.github.com/users/"+args[2]+"/repos";
		mc = new MetadataCacher(url + "?page=" + pageNumber, username, password);
		
		}
		System.out.println("Getting metadat from:"+mc.getUrl());
		String id = "";
		if (mc.authenticate()) {
			while (true) {
				if(args[1].equals("*")){
					mc.getResponseJson();
					pageContent = mc.getContent();
					if (pageContent.equals("[]"))
						break;
					if (!pageContent.isEmpty()) {
						File file=new File(outDir+pageNumber+".json");
						FileOutputStream fop=new FileOutputStream(file); 
						fop.write(pageContent.getBytes());
						fop.flush();
						fop.close();
						pageNumber++;
						mc = new MetadataCacher(url + "?page=" + pageNumber, username, password);
					}
				}else{
					boolean condition=true;
					mc.getResponseJson();
					pageContent = mc.getContent();
					if (pageContent.equals("[]"))
						break;
					if (!pageContent.isEmpty()) {
						File file=new File(outDir+pageNumber+".json");
						FileOutputStream fop=new FileOutputStream(file); 
						fop.write(pageContent.getBytes());
						fop.flush();
						fop.close();
						break;
					}
				
				}
			}
		}
		else {
			System.out.println("Authentication failed!");
		}
	}

	private static String getLastId(String pageContent) {
		int start = pageContent.length();
		if (start == 0) return "0";
		while (true) {
			String p = "{\"id\":";
			start = pageContent.lastIndexOf(p, start);
			int end = pageContent.indexOf(',', start);
			String s = pageContent.substring(start + p.length(), end);
			try {
				Integer.valueOf(s);
				return s;
			} catch (NumberFormatException e) {
				// try next id
			}
		}
	}

}
