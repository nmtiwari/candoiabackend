package JSONDownload.MetaDataGetter;

import java.util.HashMap;
import java.util.Scanner;

import JSONDownload.util.FileIO;

public class GetGitReposLanguage {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inPath = "C:/github";
		HashMap<String, StringBuilder> sbs = new HashMap<String, StringBuilder>();
		String content = FileIO.readStringFromFile(inPath + "/repos.csv");
		Scanner sc = new Scanner(content);
		int count = 0;
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] parts = line.split(",");
			int id = Integer.parseInt(parts[0]);
			HashMap<String, String> languages = getLanguages(inPath + "/repo-languages/" + id + ".json");
			count++;
			System.out.println(count + ": " + id + ": " + languages);
			for (String lang : languages.keySet()) {
				if (lang.equals("Java"))
					System.out.println();
				StringBuilder sb = sbs.get(lang);
				if (sb == null) {
					sb = new StringBuilder();
					sbs.put(lang, sb);
				}
				sb.append(line + "\r\n");
			}
		}
		sc.close();
		//for getting only Java
		{
			String lang = "Java";
			FileIO.writeStringToFile(sbs.get(lang).toString(), inPath + "/repos-" + lang + ".csv");
		}
		//for getting all languages
		/*for (String lang : sbs.keySet()) {
			FileIO.writeStringToFile(sbs.get(lang).toString(), inPath + "/repos-" + lang + ".csv");
		}*/
	}

	private static HashMap<String, String> getLanguages(String langJsonPath) {
		HashMap<String, String> languages = new HashMap<String, String>();
		String langJson = FileIO.readStringFromFile(langJsonPath);
		if (langJson.startsWith("{") && langJson.endsWith("}")) {
			int l = langJson.length();
			langJson = langJson.substring(1, l - 1);
			if (!langJson.isEmpty()) {
				String[] parts = langJson.split(",");
				for (String part : parts) {
					String[] ps = part.split(":");
					String lang = ps[0];
					if (lang.startsWith("\""))
						lang = lang.substring(1, lang.length() - 1);
					languages.put(lang, ps[1]);
				}
			}
		}
		return languages;
	}

}
