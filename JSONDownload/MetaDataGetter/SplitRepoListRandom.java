package JSONDownload.MetaDataGetter;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

import JSONDownload.util.FileIO;

public class SplitRepoListRandom {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int size = 2000;
		String inPath = "C:/github";
		if (args != null && args.length > 0)
			inPath = args[0];
		String content = FileIO.readStringFromFile(inPath + "/repos-Java-git.csv");
		int c = 0;
		Scanner sc = new Scanner(content);
		ArrayList<String> list = new ArrayList<String>();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			list.add(line);
		}
		sc.close();
		Random rand = new Random();
		int n = -1;
		StringBuilder sb = null;
		while (!list.isEmpty()) {
			c++;
			sb = new StringBuilder();
			for (int i = 0; i < size && !list.isEmpty(); i++) {
				n = rand.nextInt(list.size());
				sb.append(list.get(n) + "\n");
				list.remove(n);
			}
			FileIO.writeStringToFile(sb.toString(), inPath + "/splits/repos-Java-git-" + c + ".csv");
		}
	}

}
