package JSONDownload.MetaDataGetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Comparator;

import JSONDownload.util.FileIO;

public class ListGithubRepositories {

	private static int index = -1;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inPath = "C:/github";
		if (args != null && args.length > 0) {
			inPath = args[0];
		}
		File dir = new File(inPath + "/metadata");
		File[] files = dir.listFiles();
		Arrays.sort(files, new Comparator<File>() {
			@Override
			public int compare(File f1, File f2) {
				int n1 = getNumber(f1.getName()), n2 = getNumber(f2.getName());
				return n1 - n2;
			}

			private int getNumber(String name) {
				String s = name.substring(5, name.length() - 5);
				return Integer.valueOf(s);
			}
		});
		PrintStream out = null;
		try {
			out = new PrintStream(new FileOutputStream(inPath + "/repos.csv"));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for (File file : files) {
			String l = list(file);
			out.print(l);
		}
	}

	private static String list(File file) {
		StringBuilder sb = new StringBuilder();
		String content = FileIO.readStringFromFile(file.getAbsolutePath());
		index  = -1;
		while (true) {
			String repo = getRepo(content);
			if (repo == null)
				break;
			else
				sb.append(repo + "\r\n");
		}
		return sb.toString();
	}
	
	private static String getRepo(String content) {
		String id = getField(content, "{\"id\":");
		if (id == null) {
			return null;
		}
		System.out.println(id);
		String fullName = getField(content, ",\"full_name\":");
		String fork = getField(content, ",\"fork\":");
		
		return id + "," + fullName + "," + fork;
	}

	private static String getField(String content, String fieldName) {
		int start = content.indexOf(fieldName, index + 1);
		if (start == -1) {
			index = -1;
			return null;
		}
		int end = content.indexOf(',', start + fieldName.length());
		String s = content.substring(start + fieldName.length(), end);
		index = end;
		return s;
	}
}
