package JSONDownload.MetaDataGetter;

import java.util.Scanner;

import JSONDownload.util.FileIO;

public class GetGitList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inPath = "C:/github";
		StringBuilder sb = new StringBuilder();
		String content = FileIO.readStringFromFile(inPath + "/repos-Java.csv");
		Scanner sc = new Scanner(content);
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] parts = line.split(",");
			//String url = "git://github.com/" + parts[1] + ".git";
			sb.append(parts[1].substring(1, parts[1].length() - 1) + "\r\n");
		}
		FileIO.writeStringToFile(sb.toString(), inPath + "/repos-Java-git.csv");
	}

}
