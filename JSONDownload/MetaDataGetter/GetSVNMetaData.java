package JSONDownload.MetaDataGetter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;

import br.ufpe.cin.groundhog.http.HttpModule;

import com.google.inject.Guice;

import JSONDownload.github.MetadataCacher;
import JSONDownload.util.Config;
import JSONDownload.util.FileIO;
import JSONDownload.util.TokenGenerator;
import JSONDownload.util.TokenGenerator.GithubAPI;

public class GetSVNMetaData {
	
	/**
	 * @param args
	 * @throws IOException 
	 */
	
	//username,password,project,path
	public static void getMetaData(String[] args) throws IOException {
		String outDir ="";
		String username = Config.githubUsername;
		String password = Config.githubPassword;
		TokenGenerator builder = null;
		if (args != null && args.length > 0) {
			if (args.length >= 2) {
//						 username = args[0];
//						 password = args[1];
						 outDir=args[1]+"/JSONData";
			}else{
				System.out.println("Arguments mismatch");
				return;
			}
		}
				
		String url = "http://sourceforge.net/rest/p"+"/"+args[0].toLowerCase();
				 
		
		
		String pageContent = "";
		MetadataCacher mc = new MetadataCacher(url, username, password);
		int pageNumber = 1;
		String id = "";
			while (true) {
					mc.getResponseJson();
					pageContent = mc.getContent();
					if (pageContent.equals("[]"))
						break;
					if (!pageContent.isEmpty()) {
						File file=new File(outDir+pageNumber+".json");

						FileOutputStream fop=new FileOutputStream(file); 
						fop.write(pageContent.getBytes());
						fop.flush();
						fop.close();
						break;
					}
				
			}
	}

	
	public static void main(String [] args) throws IOException{
		getMetaData(args);
	}
	
	
	
	
	private static String getLastId(String pageContent) {
		int start = pageContent.length();
		if (start == 0) return "0";
		while (true) {
			String p = "{\"id\":";
			start = pageContent.lastIndexOf(p, start);
			int end = pageContent.indexOf(',', start);
			String s = pageContent.substring(start + p.length(), end);
			try {
				Integer.valueOf(s);
				return s;
			} catch (NumberFormatException e) {
				// try next id
			}
		}
	}

}
