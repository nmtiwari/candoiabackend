package JSONDownload.MetaDataGetter;

import java.util.Scanner;

import JSONDownload.util.FileIO;

public class SplitRepoList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int size = 20000;
		String inPath = "C:/github";
		if (args != null && args.length > 0)
			inPath = args[0];
		String content = FileIO.readStringFromFile(inPath + "/repos-Java-git.csv");
		int c = 0;
		Scanner sc = new Scanner(content);
		StringBuilder sb = new StringBuilder();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			sb.append(line + "\n");
			c++;
			if (c % size == 0) {
				FileIO.writeStringToFile(sb.toString(), inPath + "/repos-Java-git-" + (c / size) + ".csv");
				sb = new StringBuilder();
			}
		}
		if (c % size != 0) {
			FileIO.writeStringToFile(sb.toString(), inPath + "/repos-Java-git-" + (c / size + 1) + ".csv");
		}
		sc.close();
	}

}
