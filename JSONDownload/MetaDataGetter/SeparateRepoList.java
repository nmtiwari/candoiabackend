package JSONDownload.MetaDataGetter;

import java.util.Scanner;

import JSONDownload.util.FileIO;

public class SeparateRepoList {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		String inPath = "C:/github";
		String content = FileIO.readStringFromFile(inPath + "/repos.csv");
		Scanner sc = new Scanner(content);
		StringBuilder sb1 = new StringBuilder(), sb2 = new StringBuilder();
		while (sc.hasNextLine()) {
			String line = sc.nextLine();
			String[] parts = line.split(",");
			System.out.println(parts[0]);
			if (parts[2].equals("false")) {
				sb1.append(parts[0] + "," + parts[1] + "\r\n");
			}
			else {
				sb2.append(parts[0] + "," + parts[1] + "\r\n");
			}
		}
		FileIO.writeStringToFile(sb1.toString(), inPath + "/repos-org.csv");
		FileIO.writeStringToFile(sb2.toString(), inPath + "/repos-fork.csv");
	}

}
