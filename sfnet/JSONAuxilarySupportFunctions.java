package sfnet;

import JSONDownload.github.MetadataCacher;

import java.util.ArrayList;

import net.sf.json.JSONException;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;
import boa.types.Shared.Person;
import boa.types.Shared.PersonOrBuilder;
import boa.types.Shared.Person.Builder;

public class JSONAuxilarySupportFunctions {

	public JSONAuxilarySupportFunctions() {
		// TODO Auto-generated constructor stub
	}

	
	public static void fillOwner(String owner,ArrayList<Person>followers,ArrayList<Person>following,ArrayList<String>organizations){
		net.sf.json.JSONObject json = null;
		try {
			json = (net.sf.json.JSONObject) JSONSerializer.toJSON(owner);
			String followersUrl=null;
			String followingUrl=null;
			String OrganizationUrl=null;
			if(json.has("followers_url")){
				followersUrl=json.getString("followers_url");
				getAndFillPersonDetails(followersUrl,followers);
			}
			if(json.has("following_url")){
				followingUrl=json.getString("following_url");
				followingUrl=followingUrl.substring(0, followingUrl.length()-13);
				getAndFillPersonDetails(followingUrl,following);
			}
			if(json.has("organizations_url")){
				OrganizationUrl=json.getString("organizations_url");
				getAndFillOrgDetails(OrganizationUrl,organizations);
			}
		} catch (JSONException e) { 
			e.printStackTrace();
		}

	}
	
	public static void getAndFillPersonDetails(String url,ArrayList<Person>requestToBeFilled){
		String reposnseFromUrl=null;
		net.sf.json.JSONArray jsonData = null;
		MetadataCacher mc = new MetadataCacher(url, "nmtiwari", "swanit*49912");
		if(mc.authenticate()){
			try {
				mc.getResponse();
				reposnseFromUrl=mc.getContent();
				jsonData = (net.sf.json.JSONArray) JSONSerializer.toJSON(reposnseFromUrl);
				for(Object o:jsonData){
					JSONObject obj=(JSONObject)o;
					Person.Builder p=Person.newBuilder();
					if(obj.has("login")){
						p.setUsername(obj.getString("login"));
						p.setRealName(obj.getString("login"));
					}
					if(obj.has("html_url")){
						p.setEmail(obj.getString("html_url"));
					}
					requestToBeFilled.add(p.build());
				}
				
				
			} catch (Exception e) {
				
			}
		}

	}
	
	public static void getAndFillOrgDetails(String url,ArrayList<String>requestToBeFilled){

		String reposnseFromUrl=null;
		net.sf.json.JSONArray jsonData = null;
		try {
			reposnseFromUrl=HttpURLConnect.getHttpData("get", url);
			jsonData = (net.sf.json.JSONArray) JSONSerializer.toJSON(reposnseFromUrl);
			for(Object o:jsonData){
				JSONObject obj=(JSONObject)o;
				if(obj.has("login"))
				requestToBeFilled.add(obj.getString("login"));
			}
			
			
		} catch (Exception e) {
			System.out.println("Error 403 occured while readnig organizations detail");
			requestToBeFilled=new ArrayList<String>();
		}
	
	}
}
