package sfnet;

import java.io.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.api.errors.InvalidRemoteException;
import org.eclipse.jgit.api.errors.TransportException;

import JSONDownload.MetaDataGetter.GetGithubMetadata;
import boa.types.Issues;
import boa.types.Issues.Issue;
import boa.types.Issues.IssueRepository;
import boa.types.Code.CodeRepository;
import boa.types.Code.CodeRepository.RepositoryKind;
import boa.types.Shared.Person;
import boa.types.Toplevel.Project;
import boa.types.Toplevel.Project.ForgeKind;

import com.google.protobuf.CodedInputStream;

import main.DatasetGenerator;
import main.RepositoryCloner;
import main.SVNRepoCloner;
import net.sf.json.*;
import util.FileIO;
import util.ImportBugzillaReports;
import util.ImportGitIssues;
import util.ImportJiraIssues;
import util.ImportSVNTickets;
import util.Properties;

/**
 * @author hoan
 */
public class JSONSVNProjectCacher {
	private static final boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);
	private static Configuration conf=null;
	private static FileSystem fileSystem=null;
	private static SequenceFile.Writer writer=null,astWriter=null;
	private static Project prj;
	private static String ownerName;
	private static String projName=null;
	private static String projUrl=null;
	public static Project readJSONProject(File file, File cache,String path,String bugzilla,String jira,String product,String protocol,String linkForCode) {
		try{
			if((conf == null))
				conf=new Configuration();
			if(fileSystem==null)
				fileSystem=FileSystem.get(conf);
			if(writer==null)
				writer=SequenceFile.createWriter(fileSystem, conf, new Path(path+"/projects-small.seq"), Text.class, BytesWritable.class);
			if(astWriter==null)
				astWriter=SequenceFile.createWriter(fileSystem, conf, new Path(path+"/ast"), Text.class, BytesWritable.class);
		}catch(IOException ex){
			ex.printStackTrace();
		}
		
		// if a valid cache exists, use that instead of parsing
		if (cache != null && cache.exists() && file.lastModified() < cache.lastModified())
			try {
				BufferedInputStream in = new BufferedInputStream(new FileInputStream(cache));
				byte[] bytes = new byte[(int) cache.length()];
				in.read(bytes);
				in.close();
				System.err.println("using cache: " + cache.getName());
				return Project.parseFrom(CodedInputStream.newInstance(bytes, 0, bytes.length));
			} catch (IOException e) { }


		String jsonTxt = FileIO.readFileContents(file);
		if (jsonTxt.isEmpty()) {
			if (debug)
				System.err.println("Error reading file " + file.getAbsolutePath());
			return null;
		}
		
		JSONArray json = null;
		JSONObject jsonO=null;
		try {
			json = (JSONArray) JSONSerializer.toJSON(jsonTxt);
		} catch (JSONException ex) { }
		catch(java.lang.ClassCastException ex){
			jsonO=(JSONObject) JSONSerializer.toJSON(jsonTxt);

			JSONObject jsonProject=(JSONObject)jsonO;
			if (jsonProject == null || jsonProject.isNullObject()) {
				if (debug)
					System.err.println("Error reading project from file " + file.getAbsolutePath());
				return null;
			}

			Project.Builder project = Project.newBuilder();
			project.setKind(ForgeKind.SOURCEFORGE);
			if (jsonProject.has("name")){
				project.setName(jsonProject.getString("name"));
				
			}
			if(jsonProject.has("url")){
				String name=jsonProject.getString("url");
				System.out.println(name);
				name=name.substring(0, name.length()-1);
				if(name.length()>0){
					int location=name.lastIndexOf('/');
					projName=name.substring(location+1, name.length());
					System.out.println(projName);
				}
				project.setProjectUrl(jsonProject.getString("url"));
				protocol="svn";
				System.out.println("protocol set");
			if(protocol.equals("svn")){
				String[] clonePath={linkForCode,path+"/CandoiaGit/"+projName};
				System.out.println("http://svn.code.sf.net/p/"+projName.toLowerCase()+"/svn");
				projUrl=jsonProject.getString("url");
				try {
					//SVNRepoCloner.main(clonePath);  
					//String cmd="rsync  -av svn.code.sf.net::p/"+linkForCode+" "+clonePath[1];
					String cmd="rsync  -av svn.code.sf.net::p/jedit/svn /Users/nmtiwari/lab/test/CandoiaGit/Jedit";
					System.out.println("cloning set");
					Process p=Runtime.getRuntime().exec(cmd);
					p.waitFor();
					DatasetGenerator gen=new DatasetGenerator(clonePath[1]);
					gen.generate(project, astWriter);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
				
				else{
					System.out.println("going to clone through git");
					project.setProjectUrl(jsonProject.getString("url"));
					String[] clonePath={"http://git.code.sf.net/p/"+linkForCode,path+"/CandoiaGit/"+jsonProject.getString("name")};
					projUrl=jsonProject.getString("url");
					try {
						System.out.println("goiing to clone through git");
						RepositoryCloner.clone(clonePath);  
						DatasetGenerator gen=new DatasetGenerator(clonePath[1]);
						gen.generate(project, astWriter);
					} catch (InvalidRemoteException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (TransportException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (GitAPIException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				
				}
			}
				
			else
				project.setProjectUrl("Local Value");
			if (jsonProject.has("_id"))
				project.setId(jsonProject.getString("_id"));
			if (jsonProject.has("external_homepage"))
				project.setHomepageUrl(jsonProject.getString("external_homepage"));
			if (jsonProject.has("summary"))
				project.setProjectUrl(jsonProject.getString("summary"));
			if (jsonProject.has("creation_date")) {
				String timestamp = jsonProject.getString("creation_date");
				DateFormat format=new SimpleDateFormat("YYYY-MM-DD");
				Date date=null;
				try{
			        date = format.parse(timestamp);
			    }
			    catch ( Exception exce ){
			        System.out.println(ex);
			    }
				project.setCreatedDate(date.getTime() * 1000000);
			} else {
				project.setCreatedDate(-1);
			}
			if(jsonProject.has("owner")){
				Person.Builder person=Person.newBuilder();
				JSONObject jsonPerson =(JSONObject) JSONSerializer.toJSON(jsonProject.getString("owner"));
				if(jsonPerson.has("login")){
					JSONSVNProjectCacher.ownerName=jsonPerson.getString("login");
					person.setRealName(JSONSVNProjectCacher.ownerName);
					person.setUsername(JSONSVNProjectCacher.ownerName);
				}else{
					person.setRealName(jsonPerson.getString("unknown"));
					person.setUsername(jsonPerson.getString("unknown"));
				}
				if(jsonPerson.has("html_url")){
					person.setEmail(jsonPerson.getString("html_url"));
				}
				ArrayList<Person>followers=new ArrayList<Person>();
				ArrayList<Person>following=new ArrayList<Person>();
				ArrayList<String>organizations=new ArrayList<String>();
				JSONAuxilarySupportFunctions.fillOwner(jsonProject.getString("owner"), followers, following, organizations);
				person.addAllFollowers(followers);
				person.addAllFollowings(following);
				person.addAllOrganizations(organizations);
				project.addDevelopers(person.build());
			}
			if (jsonProject.has("short_description"))
				project.setDescription(jsonProject.getString("short_description"));
			if (jsonProject.has("os")) {
				JSONArray jsonOSes = jsonProject.getJSONArray("os");
				if (jsonOSes != null && jsonOSes.isArray())
					for (int i = 0; i < jsonOSes.size(); i++)
						project.addOperatingSystems(jsonOSes.getString(i));
			}
			if (jsonProject.has("language")) {
//				ArrayList<String> langs = new ArrayList<String>();
//				JSONArray languages = jsonProject.getJSONArray("programming-languages");
//				if (languages.isArray())
//					for (int i = 0; i < languages.size(); i++)
//						langs.add(languages.getString(i).trim().toLowerCase());
//				if (!langs.isEmpty())
//				project.addAllProgrammingLanguages(langs);
				String lang=jsonProject.getString("language");
				project.addProgrammingLanguages(lang);
				
			}
			if (jsonProject.has("databases")) {
				JSONArray jsonDBs = jsonProject.getJSONArray("databases");
				if (jsonDBs.isArray())
					for (int i = 0; i < jsonDBs.size(); i++)
						project.addDatabases(jsonDBs.getString(i).trim());
			}
			if (jsonProject.has("licenses")) {
				ArrayList<String> strLicenses = new ArrayList<String>();
				JSONArray licenses = jsonProject.getJSONArray("licenses");
				if (licenses.isArray())
					for (int i = 0; i < licenses.size(); i++) {
						JSONObject license = licenses.getJSONObject(i);
						if (license.has("name"))
							strLicenses.add(license.getString("name"));
					}
				if (!strLicenses.isEmpty())
					project.addAllLicenses(strLicenses);
			}
			if (jsonProject.has("topics")) {
				ArrayList<String> strTopics = new ArrayList<String>();
				JSONArray topics = jsonProject.getJSONArray("topics");
				if (topics.isArray())
					for (int i = 0; i < topics.size(); i++)
						strTopics.add(topics.getString(i).trim().toLowerCase());
				if (!strTopics.isEmpty())
					project.addAllTopics(strTopics);
			}
			if (jsonProject.has("audiences")) {
				JSONArray jsonAudiences = jsonProject.getJSONArray("audiences");
				if (jsonAudiences.isArray())
					for (int i = 0; i < jsonAudiences.size(); i++)
						project.addAudiences(jsonAudiences.getString(i).trim());
			}
			if (jsonProject.has("environments")) {
				JSONArray jsonEnvs = jsonProject.getJSONArray("environments");
				if (jsonEnvs.isArray())
					for (int i = 0; i < jsonEnvs.size(); i++)
						project.addInterfaces(jsonEnvs.getString(i).trim());
			}
			if (jsonProject.has("topics")) {
				JSONArray jsonTopics = jsonProject.getJSONArray("topics");
				if (jsonTopics.isArray())
					for (int i = 0; i < jsonTopics.size(); i++)
						project.addTopics(jsonTopics.getString(i).trim());
			}
			if (jsonProject.has("donation")) {
				JSONObject jsonDonation = jsonProject.getJSONObject("donation");
				String status = jsonDonation.getString("status");
				if (status.equals("Not Accepting"))
					project.setDonations(false);
				else if (status.equals("Accepting"))
					project.setDonations(true);
			}
			if (jsonProject.has("maintainers")) {
				ArrayList<Person> persons = new ArrayList<Person>();
				JSONArray maintainers = jsonProject.getJSONArray("maintainers");
				if (maintainers.isArray())
					for (int i = 0; i < maintainers.size(); i++) {
						JSONObject maintainer = maintainers.getJSONObject(i);
						if (maintainer.has("name")) {
							Person.Builder person = Person.newBuilder();
							person.setRealName(maintainer.getString("name"));
							person.setUsername(maintainer.getString("name"));
							person.setEmail(maintainer.getString("homepage"));
							persons.add(person.build());
						}
					}
				if (!persons.isEmpty())
					project.addAllMaintainers(persons);
			}
			if (jsonProject.has("developers")) {
				ArrayList<Person> persons = new ArrayList<Person>();
				JSONArray developers = jsonProject.getJSONArray("developers");
				if (developers.isArray())
					for (int i = 0; i < developers.size(); i++) {
						JSONObject developer = developers.getJSONObject(i);
						if (developer.has("name")) {
							Person.Builder person = Person.newBuilder();
							person.setRealName(developer.getString("name"));
							person.setUsername(developer.getString("name"));
							if(developer.has("homepage"))
							person.setEmail(developer.getString("homepage"));
							else
								person.setEmail("not found");
							persons.add(person.build());
						}
					}
				if (!persons.isEmpty())
					project.addAllDevelopers(persons);
			}
			try{
				if(projName != null && projUrl != null){
				 ImportGitIssues.getIssuesWithBuilder(project,JSONSVNProjectCacher.ownerName ,projName, projUrl, 0);
				 System.out.println("Added issues");
				}
			}catch( java.lang.IllegalStateException e){
				System.out.println("Exception occured but dont do anything");
			}
				try{
					String [] details={projName.toLowerCase()};
					System.out.println(projName.toLowerCase());
					ImportSVNTickets.getIssuesWithBuilder(project, details);
				}catch(Exception e){
					e.printStackTrace();
				}
				if((!bugzilla.equalsIgnoreCase("null")) && (!product.equalsIgnoreCase("null"))){
					ImportBugzillaReports.getIssuesWithBuilder(project,bugzilla,product);
				}
				if((!jira.equalsIgnoreCase("null"))&&(!product.equalsIgnoreCase("null"))){
					ImportJiraIssues.getIssuesWithBuilder(project,jira,product);
				}
				
			if (jsonProject.has("SVNRepository")) {
				JSONObject rep = jsonProject.getJSONObject("SVNRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.SVN);
					project.addCodeRepositories(cr.build());
				}
			}
			if (jsonProject.has("CVSRepository")) {
				JSONObject rep = jsonProject.getJSONObject("CVSRepository");
				if (rep.has("browse")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("browse"));
					cr.setKind(RepositoryKind.CVS);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("GitRepository")) {
				JSONObject rep = jsonProject.getJSONObject("GitRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.GIT);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("BzrRepository")) {
				JSONObject rep = jsonProject.getJSONObject("BzrRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.BZR);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("HgRepository")) {
				JSONObject rep = jsonProject.getJSONObject("HgRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.HG);
					project.addCodeRepositories(cr.build());
				}
			}
			prj = project.build();
			if (cache != null)
				try {
					writer.append(new Text(prj.getId()), new BytesWritable(prj.toByteArray()));
					
				} catch (IOException e) {e.printStackTrace(); }
			catch (java.lang.NullPointerException e) { }
			closeWriters();
			return prj;
		}

		if (json == null) {
			if (debug)
				System.err.println("Error parsing file " + file.getAbsolutePath());
			return null;
		}

		for(Object obj:json){
			jsonO=(JSONObject) JSONSerializer.toJSON(jsonTxt);

			JSONObject jsonProject=(JSONObject)jsonO;
			if (jsonProject == null || jsonProject.isNullObject()) {
				if (debug)
					System.err.println("Error reading project from file " + file.getAbsolutePath());
				return null;
			}

			Project.Builder project = Project.newBuilder();
			project.setKind(ForgeKind.GITHUB);
			if (jsonProject.has("name")){
				project.setName(jsonProject.getString("name"));
				projName=jsonProject.getString("name");
			}
			if(protocol.equals("svn")){
			if(jsonProject.has("url")){
				String[] clonePath={linkForCode,path+"/CandoiaGit/"+projName};
				System.out.println("http://svn.code.sf.net/p/"+projName.toLowerCase()+"/svn");
				projUrl=jsonProject.getString("url");
				try {
					//SVNRepoCloner.main(clonePath);  
					String cmd="rsync  -av svn.code.sf.net::p/"+linkForCode+" "+clonePath[1];
					Process p=Runtime.getRuntime().exec(cmd);
					p.waitFor();
					DatasetGenerator gen=new DatasetGenerator(clonePath[1]);
					gen.generate(project, astWriter);
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			}
			else{
				System.out.println("going to clone through git");
				project.setProjectUrl(jsonProject.getString("url"));
				String[] clonePath={"http://git.code.sf.net/p/"+linkForCode,path+"/CandoiaGit/"+jsonProject.getString("name")};
				projUrl=jsonProject.getString("url");
				try {
					System.out.println("goiing to clone through git");
					RepositoryCloner.clone(clonePath);  
					DatasetGenerator gen=new DatasetGenerator(clonePath[1]);
					gen.generate(project, astWriter);
				} catch (InvalidRemoteException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (TransportException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (GitAPIException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
				project.setProjectUrl("Local Value");
			if (jsonProject.has("_id"))
				project.setId(jsonProject.getString("_id"));
			if (jsonProject.has("externa_homepage"))
				project.setHomepageUrl(jsonProject.getString("externa_homepage"));
			if (jsonProject.has("summary"))
				project.setProjectUrl(jsonProject.getString("summary"));
			if (jsonProject.has("creation_date")) {
				String timestamp = jsonProject.getString("creation_date");
				DateFormat format=new SimpleDateFormat("YYYY-MM-DD");
				Date date=null;
				try{
			        date = format.parse(timestamp);
			    }
			    catch ( Exception exce ){
			        exce.printStackTrace();
			    }
				project.setCreatedDate(date.getTime() * 1000000);
			} else {
				project.setCreatedDate(-1);
			}
			if(jsonProject.has("owner")){
				Person.Builder person=Person.newBuilder();
				JSONObject jsonPerson =(JSONObject) JSONSerializer.toJSON(jsonProject.getString("owner"));
				if(jsonPerson.has("login")){
					JSONSVNProjectCacher.ownerName=jsonPerson.getString("login");
					person.setRealName(JSONSVNProjectCacher.ownerName);
					person.setUsername(JSONSVNProjectCacher.ownerName);
				}else{
					person.setRealName(jsonPerson.getString("unknown"));
					person.setUsername(jsonPerson.getString("unknown"));
				}
				if(jsonPerson.has("html_url")){
					person.setEmail(jsonPerson.getString("html_url"));
				}
				ArrayList<Person>followers=new ArrayList<Person>();
				ArrayList<Person>following=new ArrayList<Person>();
				ArrayList<String>organizations=new ArrayList<String>();
				JSONAuxilarySupportFunctions.fillOwner(jsonProject.getString("owner"), followers, following, organizations);
				person.addAllFollowers(followers);
				person.addAllFollowings(following);
				person.addAllOrganizations(organizations);
				project.addDevelopers(person.build());
			}
			if (jsonProject.has("short_description"))
				project.setDescription(jsonProject.getString("short_description"));
			if (jsonProject.has("os")) {
				JSONArray jsonOSes = jsonProject.getJSONArray("os");
				if (jsonOSes != null && jsonOSes.isArray())
					for (int i = 0; i < jsonOSes.size(); i++)
						project.addOperatingSystems(jsonOSes.getString(i));
			}
			if (jsonProject.has("language")) {
//				ArrayList<String> langs = new ArrayList<String>();
//				JSONArray languages = jsonProject.getJSONArray("programming-languages");
//				if (languages.isArray())
//					for (int i = 0; i < languages.size(); i++)
//						langs.add(languages.getString(i).trim().toLowerCase());
//				if (!langs.isEmpty())
//				project.addAllProgrammingLanguages(langs);
				String lang=jsonProject.getString("language");
				project.addProgrammingLanguages(lang);
				
			}
			if (jsonProject.has("databases")) {
				JSONArray jsonDBs = jsonProject.getJSONArray("databases");
				if (jsonDBs.isArray())
					for (int i = 0; i < jsonDBs.size(); i++)
						project.addDatabases(jsonDBs.getString(i).trim());
			}
			if (jsonProject.has("licenses")) {
				ArrayList<String> strLicenses = new ArrayList<String>();
				JSONArray licenses = jsonProject.getJSONArray("licenses");
				if (licenses.isArray())
					for (int i = 0; i < licenses.size(); i++) {
						JSONObject license = licenses.getJSONObject(i);
						if (license.has("name"))
							strLicenses.add(license.getString("name"));
					}
				if (!strLicenses.isEmpty())
					project.addAllLicenses(strLicenses);
			}
			if (jsonProject.has("topics")) {
				ArrayList<String> strTopics = new ArrayList<String>();
				JSONArray topics = jsonProject.getJSONArray("topics");
				if (topics.isArray())
					for (int i = 0; i < topics.size(); i++)
						strTopics.add(topics.getString(i).trim().toLowerCase());
				if (!strTopics.isEmpty())
					project.addAllTopics(strTopics);
			}
			if (jsonProject.has("audiences")) {
				JSONArray jsonAudiences = jsonProject.getJSONArray("audiences");
				if (jsonAudiences.isArray())
					for (int i = 0; i < jsonAudiences.size(); i++)
						project.addAudiences(jsonAudiences.getString(i).trim());
			}
			if (jsonProject.has("environments")) {
				JSONArray jsonEnvs = jsonProject.getJSONArray("environments");
				if (jsonEnvs.isArray())
					for (int i = 0; i < jsonEnvs.size(); i++)
						project.addInterfaces(jsonEnvs.getString(i).trim());
			}
			if (jsonProject.has("topics")) {
				JSONArray jsonTopics = jsonProject.getJSONArray("topics");
				if (jsonTopics.isArray())
					for (int i = 0; i < jsonTopics.size(); i++)
						project.addTopics(jsonTopics.getString(i).trim());
			}
			if (jsonProject.has("donation")) {
				JSONObject jsonDonation = jsonProject.getJSONObject("donation");
				String status = jsonDonation.getString("status");
				if (status.equals("Not Accepting"))
					project.setDonations(false);
				else if (status.equals("Accepting"))
					project.setDonations(true);
			}
			if (jsonProject.has("maintainers")) {
				ArrayList<Person> persons = new ArrayList<Person>();
				JSONArray maintainers = jsonProject.getJSONArray("maintainers");
				if (maintainers.isArray())
					for (int i = 0; i < maintainers.size(); i++) {
						JSONObject maintainer = maintainers.getJSONObject(i);
						if (maintainer.has("name")) {
							Person.Builder person = Person.newBuilder();
							person.setRealName(maintainer.getString("name"));
							person.setUsername(maintainer.getString("name"));
							person.setEmail(maintainer.getString("homepage"));
							persons.add(person.build());
						}
					}
				if (!persons.isEmpty())
					project.addAllMaintainers(persons);
			}
			if (jsonProject.has("developers")) {
				ArrayList<Person> persons = new ArrayList<Person>();
				JSONArray developers = jsonProject.getJSONArray("developers");
				if (developers.isArray())
					for (int i = 0; i < developers.size(); i++) {
						JSONObject developer = developers.getJSONObject(i);
						if (developer.has("name")) {
							Person.Builder person = Person.newBuilder();
							person.setRealName(developer.getString("name"));
							person.setUsername(developer.getString("name"));
							if(developer.has("homepage"))
							person.setEmail(developer.getString("homepage"));
							else
								person.setEmail("not found");
							persons.add(person.build());
						}
					}
				if (!persons.isEmpty())
					project.addAllDevelopers(persons);
			}
			try{
				if(projName != null && projUrl != null){
				 ImportGitIssues.getIssuesWithBuilder(project,JSONSVNProjectCacher.ownerName ,projName, projUrl, 0);
				 System.out.println("Added issues");
				}
			}catch( java.lang.IllegalStateException e){
				System.out.println("Exception occured but dont do anything");
			}
				try{
					String [] details={projName.toLowerCase()};
					System.out.println(projName.toLowerCase());
					ImportSVNTickets.getIssuesWithBuilder(project, details);
				}catch(Exception e){
					e.printStackTrace();
				}
				if((!bugzilla.equalsIgnoreCase("null")) && (!product.equalsIgnoreCase("null"))){
					ImportBugzillaReports.getIssuesWithBuilder(project,bugzilla,product);
				}
				if((!jira.equalsIgnoreCase("null"))&& (!product.equalsIgnoreCase("null"))){
					ImportJiraIssues.getIssuesWithBuilder(project,jira,product);
				}
			if (jsonProject.has("SVNRepository")) {
				JSONObject rep = jsonProject.getJSONObject("SVNRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.SVN);
					project.addCodeRepositories(cr.build());
				}
			}
			if (jsonProject.has("CVSRepository")) {
				JSONObject rep = jsonProject.getJSONObject("CVSRepository");
				if (rep.has("browse")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("browse"));
					cr.setKind(RepositoryKind.CVS);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("GitRepository")) {
				JSONObject rep = jsonProject.getJSONObject("GitRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.GIT);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("BzrRepository")) {
				JSONObject rep = jsonProject.getJSONObject("BzrRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.BZR);
					project.addCodeRepositories(cr.build());
				}
			}
			// FIXME verify key name
			if (jsonProject.has("HgRepository")) {
				JSONObject rep = jsonProject.getJSONObject("HgRepository");
				if (rep.has("location")) {
					CodeRepository.Builder cr = CodeRepository.newBuilder();
					cr.setUrl(rep.getString("location"));
					cr.setKind(RepositoryKind.HG);
					project.addCodeRepositories(cr.build());
				}
			}
			prj = project.build();
			if (cache != null)
				try {
					writer.append(new Text(prj.getId()), new BytesWritable(prj.toByteArray()));
					
				} catch (IOException e) {e.printStackTrace(); }
			catch (java.lang.NullPointerException e) { }
			closeWriters();
			return prj;
		}
		
		closeWriters();
		return prj;
	}
	
    public static void closeWriters() {
        while (true) {
            try {
                writer.close();
                astWriter.close();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }
}
