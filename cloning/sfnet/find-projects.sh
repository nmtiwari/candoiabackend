#!/bin/bash

i=0

if [ $# -eq 1 ]; then
	i=$(($1-1))
fi

while true
do
	i=$((i+1))
	echo "Processing page $i"
	wget "http://sourceforge.net/directory/?sort=update&page=$i" -q -O $i.page.html

	# process the page to find new projects and get their JSON data
	for j in `grep '<header><a href="/projects/' $i.page.html |cut -d/ -f3|sort|uniq`
	do
		./get-project.sh $j
	done

	# found the last page, stop script
	grep -q '">Next</a>' $i.page.html
	if [ $? -ne 0 ]; then
		break
	fi

	rm -f $i.page.html
	sleep 1
done

rm -f *.page.html
