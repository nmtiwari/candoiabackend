#!/bin/bash

for i in `find -name '*.json.txt'`
do
	i=`echo $i|cut -d/ -f2|rev|cut -d. -f3-|rev`
	./get-project.sh $i -update
done
