#!/bin/bash

for i in `find -name '*.json.txt' -exec grep -o '"username": "[^"]*"' '{}' ';'|cut -d\" -f4|sort|uniq`
do
	./get-project.sh $i.u -update
done
