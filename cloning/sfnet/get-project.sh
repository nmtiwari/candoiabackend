#!/bin/bash

echo $1|grep -q '\.'

if [ $? -eq 0 ]; then
	url=""
	array=(${1//\./ })
	for i in "${!array[@]}"
	do
		url="${array[$i]}/$url"
	done
else
	url="p/$1/"
fi

if [ -f $1.json.txt ] && [ "-update" != "$2" ] ; then
	echo "Skipping project $1"
else
	if [ "-update" == "$2" ]; then
		echo "Updating project $1"
	else
		echo "Found new project $1"
	fi
echo "nitin"+$url;
	wget http://sourceforge.net/rest/$url -q -O $1.json.txt

	if [ $? -ne 0 ]; then
		echo "Error fetching project $1"
		rm -f $1.json.txt
	fi
fi
