#!/bin/sh

for i in `cat $1` ; do
	echo "Cloning $i..."
	if [ ! -d $i ] ; then
		mkdir $i
	fi

	wget -q --spider "http://svn.code.sf.net/p/$i/code/"
	if [ $? -eq 0 ] ; then
		rsync --delete -avzP svn.code.sf.net::p/$i/code/ $i
	else
		wget -q --spider "http://svn.code.sf.net/p/$i/svn/"
		if [ $? -eq 0 ] ; then
			rsync --delete -avzP svn.code.sf.net::p/$i/svn/ $i
		else
			rsync -avzP --delete $i.svn.sourceforge.net::svn/$i/* $i
		fi
	fi
done
