package util;

import java.io.IOException;

import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.HTablePool;
import org.apache.hadoop.hbase.client.Put;

public class HBaseUtil {
	private final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);

	public static HTableInterface getTable(HTablePool tablePool, HTableInterface table, byte[] tableName) {
		if (table != null) {
			try {
				table.close();
			} catch (final IOException e) {
				printError(e, "error closing table");
			}
			try {
				Thread.sleep(1000);
			} catch (final Exception e) {}
		}

		table = tablePool.getTable(tableName);

		// allow Puts to buffer to improve import speed
		//astTable.setAutoFlush(false, true);
		try {
			table.setWriteBufferSize(12*1024*1024);
		} catch (final IOException e) {
			printError(e, "error setting write buffer");
		}
		return table;
	}
	
	public static void put(HTablePool tablePool, HTableInterface astTable, byte[] astTableName, byte[] rowKeyBytes, byte[] family, byte[] key, byte[] value) {
		final Put revPut = new Put(rowKeyBytes);
		revPut.add(family, key, value);
		while (true) {
			try {
				astTable.put(revPut);
				break;
			} catch (final IllegalArgumentException e) {
				// probably tried inserting an empty put, ignore it
				break;
			} catch (final IOException e) {
				HBaseUtil.printError(e, "error writing table row");
				astTable = HBaseUtil.getTable(tablePool, astTable, astTableName);
			}
		}
	}

	public static void printError(final Throwable e, final String message) {
		System.err.println("ERR: " + message);
		if (debug) {
			e.printStackTrace();
			//System.exit(-1);
		}
		else
			System.err.println(e.getMessage());
	}

}
