package util;

/**
 * @author hoan
 */
public class SVNUtil {
	public static String getSVNRepoRootName(final String url) {
		int end = url.length() - 1;
		while (url.charAt(end) == '/' && end >= 0)
			end--;

		String name = "";
		if (end >= 0) {
			int start = url.lastIndexOf('/', end);
			if (start <= end)
				name = url.substring(start + 1, end + 1);
		}
		
		return name;
	}
}
