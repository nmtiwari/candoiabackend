package util;

import java.util.ArrayList;
import java.util.List;

import boa.types.Issues.Issue;
import boa.types.Issues.IssueRepository;
import br.ufpe.cin.groundhog.Project;
import br.ufpe.cin.groundhog.User;
import br.ufpe.cin.groundhog.search.SearchModule;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class ImportSVNTickets {
	static SearchSVN searchSVN;
	private static List<Issue>issues;
	public final List<boa.types.Issues.Issue> importTickets(String pname) {
		if (searchSVN == null)
			setup();

		List<Issue> issues = searchSVN.storeTickets(pname);
		//system.out.println("Total Tickets:"+issues.size());
		for(Issue i:issues)
			//system.out.println(i);
		return issues;
	}

	public static List<Issue> getSVNTickets(String[] args) {
		if (args.length < 1) {
			//system.out.println("Please provide the sourceforge project name!");
			return new ArrayList<Issue>();
		}
		ImportSVNTickets svnTickets = new ImportSVNTickets();
		return svnTickets.importTickets(args[0]);
	}

	public static void main(String[] args) {
		if (args.length < 1) {
			//system.out.println("Please provide the sourceforge project name!");
		}
		ImportSVNTickets svnTickets = new ImportSVNTickets();
		issues=getSVNTickets(args);
		for(Issue i:issues){
			//system.out.println(i);
		}
	}
	
	
	private static void setup() {
		Injector injector = Guice.createInjector(new SearchModule());
		searchSVN = injector.getInstance(SearchSVN.class);
	}

	
	public static void getIssuesWithBuilder(boa.types.Toplevel.Project.Builder project,String[] details) {
		issues=getSVNTickets(details);
		final IssueRepository.Builder issueRepoBuilder = IssueRepository.newBuilder();
		issueRepoBuilder.setUrl(details[0]);
		for (Issue issue : issues) {
			boa.types.Issues.Issue.Builder issueBuilder = boa.types.Issues.Issue
					.newBuilder();
			issueRepoBuilder.addIssues(issue);
		}
		project.addIssueRepositories(issueRepoBuilder);
	}
	
	
}
