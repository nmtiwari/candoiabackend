package util;


import org.eclipse.wst.jsdt.core.dom.ASTNode;
import org.eclipse.wst.jsdt.core.dom.ASTVisitor;

/**
 * @author rdyer
 */
public class JavaScriptErrorCheckVisitor extends ASTVisitor {
	public boolean hasError = false;

	public boolean preVisit2(ASTNode node) {
		if ((node.getFlags() & ASTNode.MALFORMED) != 0)
			hasError = true;
		return !hasError;
	}
}
