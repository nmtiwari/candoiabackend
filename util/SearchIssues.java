package util;

import java.util.ArrayList;
import java.util.List;

import util.UrlBuilder.GithubAPI;
import br.ufpe.cin.groundhog.Issue;
import br.ufpe.cin.groundhog.IssueLabel;
import br.ufpe.cin.groundhog.Project;
import br.ufpe.cin.groundhog.http.HttpModule;
import br.ufpe.cin.groundhog.http.Requests;
//import br.ufpe.cin.groundhog.search.UrlBuilder;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.inject.Guice;
import com.google.inject.Inject;
import util.UrlBuilder;

public class SearchIssues {
	private final Gson gson;
	private final Requests requests;
	private final UrlBuilder builder;
	
	@Inject
	public SearchIssues(Requests requests) {
		this.requests = requests;
		this.gson = new Gson();
		this.builder = Guice.createInjector(new HttpModule()).getInstance(UrlBuilder.class);
	}
	
	public List<Issue> getAllProjectIssues(Project project) {

		int pageNumber=1;
		boolean check=true;
		List<Issue> issues = new ArrayList<Issue>();
	
		while(true){	
			String searchUrl = builder.uses(GithubAPI.ROOT)
					  .withParam("repos")
					  .withSimpleParam("/", project.getOwner().getLogin())
					  .withSimpleParam("/", project.getName())
					  .withParam("/issues")
					  .withParam("?page="+pageNumber)
					  .build();
//		String searchUrl="https://api.github.com/repos/"+project.getOwner().getLogin()+"/"+project.getName()+"/issues?pages="+pageNumber;
		System.out.println("searcing for:"+searchUrl);
		String jsonString = requests.get(searchUrl);
		System.out.println("jsonString:"+jsonString);
		if(!jsonString.equals("[]")){
			JsonArray jsonArray = gson.fromJson(jsonString, JsonElement.class).getAsJsonArray();
			List<IssueLabel> labels = new ArrayList<IssueLabel>();
			
			for (JsonElement element : jsonArray) {
				Issue issue = gson.fromJson(element, Issue.class);
				issue.setProject(project);
				
				for (JsonElement lab : element.getAsJsonObject().get("labels").getAsJsonArray()) {
					IssueLabel label = gson.fromJson(lab, IssueLabel.class);				
					labels.add(label);
				}
				
				issue.setLabels(labels);
				issues.add(issue);
			}
			pageNumber++;
		}else{
			break;
		}
	}
		return issues;
	}
}
