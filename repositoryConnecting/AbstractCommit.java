package repositoryConnecting;

import java.io.*;
import java.util.*;

import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.HTablePool;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.SequenceFile.Writer;
import org.eclipse.jdt.core.JavaCore;
import org.eclipse.jdt.core.dom.*;
import org.eclipse.wst.jsdt.core.JavaScriptCore;
import org.eclipse.wst.jsdt.core.dom.JavaScriptUnit;

import boa.types.Ast.ASTRoot;
import boa.types.Ast.Comment;
import boa.types.Ast.CommentsRoot;
import boa.types.Code.Revision;
import boa.types.Diff.ChangedFile;
import boa.types.Diff.ChangedFile.Builder;
import boa.types.Diff.ChangedFile.FileKind;
import boa.types.Shared.ChangeKind;
import boa.types.Shared.Person;
import util.FileIO;
import util.HBaseUtil;
import util.JavaScriptErrorCheckVisitor;
import util.JavaScriptVisitor;
import util.JavaVisitor;
import util.JavaErrorCheckVisitor;
import util.Properties;

/**
 * @author rdyer
 */
public abstract class AbstractCommit {
	protected static final boolean debug = false; // util.Properties.getBoolean("debug",
													// main.DefaultProperties.DEBUG);
	private final static byte[] code_family = Bytes
			.toBytes(Properties.getProperty("hbase.ast.col",
					main.DefaultProperties.HBASE_AST_COL));
	public static int count = 0;
	protected String id;

	public void setId(final String id) {
		this.id = id;
	}

	protected String author;

	public void setAuthor(final String author) {
		this.author = author;
	}

	protected String committer;

	public void setCommitter(final String committer) {
		this.committer = committer;
	}

	protected String message;

	public void setMessage(final String message) {
		this.message = message;
	}

	protected Date date;

	public void setDate(final Date date) {
		this.date = date;
	}

	private Map<String, String> changedPaths = new HashMap<String, String>();

	public void setChangedPaths(final Map<String, String> changedPaths) {
		this.changedPaths = changedPaths;
	}

	private Map<String, String> addedPaths = new HashMap<String, String>();

	public void setAddedPaths(final Map<String, String> addedPaths) {
		this.addedPaths = addedPaths;
	}

	private Map<String, String> removedPaths = new HashMap<String, String>();

	public void setRemovedPaths(final Map<String, String> removedPaths) {
		this.removedPaths = removedPaths;
	}

	protected int[] parentIndices;

	protected void setParentIndices(final int[] parentList) {
		parentIndices = parentList;
	}

	protected int[] getParentIndices() {
		return parentIndices;
	}

	protected static final ByteArrayOutputStream buffer = new ByteArrayOutputStream(
			4096);

	protected abstract String getFileContents(final String path);

	protected abstract Person parsePerson(final String s);

	public Revision asProtobuf(final boolean parse, final Writer astWriter,
			final String repoKey, final String keyDelim) {
		final Revision.Builder revision = Revision.newBuilder();
		revision.setId(id);

		final Person author = parsePerson(this.author);
		final Person committer;
		if(this.committer!=null)
		committer = parsePerson(this.committer);
		else
			committer = parsePerson("anonymous");	
		revision.setAuthor(author == null ? committer : author);
		revision.setCommitter(committer);

		long time = -1;
		if (date != null)
			time = date.getTime() * 1000;
		revision.setCommitDate(time);

		if (message != null)
			revision.setLog(message);
		else
			revision.setLog("");

		for (final String path : changedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse,
					astWriter, repoKey + keyDelim + id, keyDelim);
			fb.setChange(ChangeKind.MODIFIED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : addedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse,
					astWriter, repoKey + keyDelim + id, keyDelim);
			fb.setChange(ChangeKind.ADDED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : removedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, false, null,
					repoKey + keyDelim + id, keyDelim);
			fb.setChange(ChangeKind.DELETED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}

		return revision.build();
	}

	public Revision asProtobuf(final boolean parse, final HTablePool tablePool,
			final HTableInterface table, final byte[] tableName,
			final String repoKey, final String keyDelim) {
		final Revision.Builder revision = Revision.newBuilder();
		revision.setId(id);

		final Person author = parsePerson(this.author);
		final Person committer = parsePerson(this.committer);
		revision.setAuthor(author == null ? committer : author);
		revision.setCommitter(committer);

		long time = -1;
		if (date != null)
			time = date.getTime() * 1000;
		revision.setCommitDate(time);

		if (message != null)
			revision.setLog(message);
		else
			revision.setLog("");

		for (final String path : changedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse,
					tablePool, table, tableName, repoKey + keyDelim + id,
					keyDelim);
			fb.setChange(ChangeKind.MODIFIED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : addedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse,
					tablePool, table, tableName, repoKey + keyDelim + id,
					keyDelim);
			fb.setChange(ChangeKind.ADDED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : removedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, false, null,
					null, null, repoKey + keyDelim + id, keyDelim);
			fb.setChange(ChangeKind.DELETED);
			// fb.setKey("");
			revision.addFiles(fb.build());
		}

		return revision.build();
	}

	private Builder processChangeFile(final String path, final boolean parse,
			final HTablePool tablePool, final HTableInterface table,
			final byte[] tableName, final String revKey, final String keyDelim) {
		final ChangedFile.Builder fb = ChangedFile.newBuilder();
		fb.setName(path);
		fb.setKind(FileKind.OTHER);

		final String lowerPath = path.toLowerCase();
		if (lowerPath.endsWith(".txt"))
			fb.setKind(FileKind.TEXT);
		else if (lowerPath.endsWith(".xml"))
			fb.setKind(FileKind.XML);
		else if (lowerPath.endsWith(".jar") || lowerPath.endsWith(".class"))
			fb.setKind(FileKind.BINARY);
		else if (lowerPath.endsWith(".java") && parse) {
			final byte[] revKeyBytes = Bytes.toBytes(revKey);
			final String content = getFileContents(path);

			fb.setKind(FileKind.SOURCE_JAVA_JLS2);
			if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_4,
					AST.JLS2, false, tablePool, table, tableName, revKeyBytes)) {
				if (debug)
					System.err.println("Found JLS2 parse error in: revision "
							+ id + ": file " + path);

				fb.setKind(FileKind.SOURCE_JAVA_JLS3);
				if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_5,
						AST.JLS3, false, tablePool, table, tableName,
						revKeyBytes)) {
					if (debug)
						System.err
								.println("Found JLS3 parse error in: revision "
										+ id + ": file " + path);

					fb.setKind(FileKind.SOURCE_JAVA_JLS4);
					if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_7,
							AST.JLS4, false, tablePool, table, tableName,
							revKeyBytes)) {
						if (debug)
							System.err
									.println("Found JLS4 parse error in: revision "
											+ id + ": file " + path);

						// fb.setContent(content);
						fb.setKind(FileKind.SOURCE_JAVA_ERROR);
						HBaseUtil.put(tablePool, table, tableName, revKeyBytes,
								code_family, Bytes.toBytes(path), ASTRoot
										.newBuilder().build().toByteArray());
					} else if (debug)
						System.err.println("Accepted JLS4: revision " + id
								+ ": file " + path);
				} else if (debug)
					System.err.println("Accepted JLS3: revision " + id
							+ ": file " + path);
			} else if (debug)
				System.err.println("Accepted JLS2: revision " + id + ": file "
						+ path);
		}
		fb.setKey(revKey);

		return fb;
	}

	private boolean parseJavaFile(final String path, final Builder fb,
			final String content, final String compliance, final int astLevel,
			final boolean storeOnError, final HTablePool tablePool,
			final HTableInterface table, final byte[] tableName,
			final byte[] keyBytes) {
		try {
			final ASTParser parser = ASTParser.newParser(astLevel);
			parser.setKind(ASTParser.K_COMPILATION_UNIT);
			parser.setResolveBindings(true);
			parser.setSource(content.toCharArray());

			final Map options = JavaCore.getOptions();
			JavaCore.setComplianceOptions(compliance, options);
			parser.setCompilerOptions(options);

			final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

			final JavaErrorCheckVisitor errorCheck = new JavaErrorCheckVisitor();
			cu.accept(errorCheck);

			if (!errorCheck.hasError || storeOnError) {
				final ASTRoot.Builder ast = ASTRoot.newBuilder();
				// final CommentsRoot.Builder comments =
				// CommentsRoot.newBuilder();
				final JavaVisitor visitor = new JavaVisitor(content);
				try {
					ast.addNamespaces(visitor.getNamespaces(cu));
					for (final String s : visitor.getImports())
						ast.addImports(s);
					/*
					 * for (final Comment c : visitor.getComments())
					 * comments.addComments(c);
					 */
				} catch (final UnsupportedOperationException e) {
					return false;
				} catch (final Exception e) {
					if (debug)
						System.err.println("Error visiting: " + path);
					e.printStackTrace();
					return false;
				}

				if (table != null) {
					HBaseUtil.put(tablePool, table, tableName, keyBytes,
							code_family, Bytes.toBytes(path), ast.build()
									.toByteArray());
				} else
					fb.setAst(ast);
				// fb.setComments(comments);
			}

			return !errorCheck.hasError;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private Builder processChangeFile(String path, boolean parse,
			Writer astWriter, String revKey, String keyDelim) {
		final ChangedFile.Builder fb = ChangedFile.newBuilder();
		fb.setName(path);
		fb.setKind(FileKind.OTHER);

		final String lowerPath = path.toLowerCase();
		if (lowerPath.endsWith(".txt"))
			fb.setKind(FileKind.TEXT);
		else if (lowerPath.endsWith(".xml"))
			fb.setKind(FileKind.XML);
		else if (lowerPath.endsWith(".jar") || lowerPath.endsWith(".class"))
			fb.setKind(FileKind.BINARY);
		else if (lowerPath.endsWith(".java") && parse) {
			final String content = getFileContents(path);

			fb.setKind(FileKind.SOURCE_JAVA_JLS2);
			if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_4,
					AST.JLS2, false, astWriter, revKey + keyDelim + path)) {
				if (debug)
					System.err.println("Found JLS2 parse error in: revision "
							+ id + ": file " + path);

				fb.setKind(FileKind.SOURCE_JAVA_JLS3);
				if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_5,
						AST.JLS3, false, astWriter, revKey + keyDelim + path)) {
					if (debug)
						System.err
								.println("Found JLS3 parse error in: revision "
										+ id + ": file " + path);

					fb.setKind(FileKind.SOURCE_JAVA_JLS4);
					if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_7,
							AST.JLS4, false, astWriter, revKey + keyDelim
									+ path)) {
						if (debug)
							System.err
									.println("Found JLS4 parse error in: revision "
											+ id + ": file " + path);

						// fb.setContent(content);
						fb.setKind(FileKind.SOURCE_JAVA_ERROR);
						try {
							astWriter.append(
									new Text(revKey + keyDelim + fb.getName()),
									new BytesWritable(ASTRoot.newBuilder()
											.build().toByteArray()));
						} catch (Exception e) {
							//e.printStackTrace();
						}
					} else if (debug)
						System.err.println("Accepted JLS4: revision " + id
								+ ": file " + path);
				} else if (debug)
					System.err.println("Accepted JLS3: revision " + id
							+ ": file " + path);
			} else if (debug)
				System.err.println("Accepted JLS2: revision " + id + ": file "
						+ path);
		} else if (lowerPath.endsWith(".js") && parse) {
			final String content = getFileContents(path);

			fb.setKind(FileKind.OTHER);
			if (!parseJavaScriptFile(path, fb, content,
					JavaScriptCore.VERSION_1_1,
					org.eclipse.wst.jsdt.core.dom.AST.JLS3, false, astWriter,
					revKey + keyDelim + path)) {
				if (debug)
					System.err.println("Found JLS3 parse error in: revision "
							+ id + ": file " + path);

				fb.setKind(FileKind.OTHER);
				if (!parseJavaScriptFile(path, fb, content,
						JavaScriptCore.VERSION_1_2,
						org.eclipse.wst.jsdt.core.dom.AST.JLS3, false,
						astWriter, revKey + keyDelim + path)) {
					if (debug)
						System.err
								.println("Found JLS3 parse error in: revision "
										+ id + ": file " + path);

					fb.setKind(FileKind.OTHER);
					if (!parseJavaScriptFile(path, fb, content,
							JavaScriptCore.VERSION_1_3,
							org.eclipse.wst.jsdt.core.dom.AST.JLS3, false,
							astWriter, revKey + keyDelim + path)) {
						if (debug)
							System.err
									.println("Found JLS3 parse error in: revision "
											+ id + ": file " + path);

						fb.setKind(FileKind.OTHER);
						if (!parseJavaScriptFile(path, fb, content,
								JavaScriptCore.VERSION_1_4,
								org.eclipse.wst.jsdt.core.dom.AST.JLS3, false,
								astWriter, revKey + keyDelim + path)) {
							if (debug)
								System.err
										.println("Found JLS3 parse error in: revision "
												+ id + ": file " + path);

							fb.setKind(FileKind.OTHER);
							if (!parseJavaScriptFile(path, fb, content,
									JavaScriptCore.VERSION_1_5,
									org.eclipse.wst.jsdt.core.dom.AST.JLS2,
									false, astWriter, revKey + keyDelim + path)) {
								if (debug)
									System.err
											.println("Found JLS2 parse error in: revision "
													+ id + ": file " + path);

								fb.setKind(FileKind.OTHER);
								if (!parseJavaScriptFile(path, fb, content,
										JavaScriptCore.VERSION_1_6,
										org.eclipse.wst.jsdt.core.dom.AST.JLS3,
										false, astWriter, revKey + keyDelim
												+ path)) {
									if (debug)
										System.err
												.println("Found JLS3 parse error in: revision "
														+ id + ": file " + path);

									fb.setKind(FileKind.OTHER);
									if (!parseJavaScriptFile(
											path,
											fb,
											content,
											JavaScriptCore.VERSION_1_7,
											org.eclipse.wst.jsdt.core.dom.AST.JLS3,
											false, astWriter, revKey + keyDelim
													+ path)) {
										if (debug)
											System.err
													.println("Found JLS4 parse error in: revision "
															+ id
															+ ": file "
															+ path);

										// fb.setContent(content);
										fb.setKind(FileKind.OTHER);
										try {
											astWriter.append(new Text(revKey
													+ keyDelim + fb.getName()),
													new BytesWritable(ASTRoot
															.newBuilder()
															.build()
															.toByteArray()));
										} catch (IOException e) {
											e.printStackTrace();
										}
										catch (NullPointerException e) {
											e.printStackTrace();
										}
									} else if (debug)
										System.err
												.println("Accepted JLS7: revision "
														+ id + ": file " + path);
								} else if (debug)
									System.err
											.println("Accepted JLS6: revision "
													+ id + ": file " + path);
							} else if (debug)
								System.err.println("Accepted JLS5: revision "
										+ id + ": file " + path);
						} else if (debug)
							System.err.println("Accepted JLS4: revision " + id
									+ ": file " + path);
					} else if (debug)
						System.err.println("Accepted JLS3: revision " + id
								+ ": file " + path);
				} else if (debug)
					System.err.println("Accepted JLS2: revision " + id
							+ ": file " + path);
			} else if (debug)
				System.err.println("Accepted JLS1: revision " + id + ": file "
						+ path);
		}
		fb.setKey(revKey);

		return fb;
	}

	public Revision asProtobuf(final boolean parse) {
		final Revision.Builder revision = Revision.newBuilder();
		revision.setId(id);

		final Person author = parsePerson(this.author);
		final Person committer = parsePerson(this.committer);
		revision.setAuthor(author == null ? committer : author);
		revision.setCommitter(committer);

		long time = -1;
		if (date != null)
			time = date.getTime() * 1000;
		revision.setCommitDate(time);

		if (message != null)
			revision.setLog(message);
		else
			revision.setLog("");

		for (final String path : changedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse);
			fb.setChange(ChangeKind.MODIFIED);
			fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : addedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, parse);
			fb.setChange(ChangeKind.ADDED);
			fb.setKey("");
			revision.addFiles(fb.build());
		}
		for (final String path : removedPaths.keySet()) {
			final ChangedFile.Builder fb = processChangeFile(path, false);
			fb.setChange(ChangeKind.DELETED);
			fb.setKey("");
			revision.addFiles(fb.build());
		}

		return revision.build();
	}

	public Map<String, String> getLOC() {
		final Map<String, String> l = new HashMap<String, String>();

		for (final String path : changedPaths.keySet())
			l.put(path, processLOC(path));
		for (final String path : addedPaths.keySet())
			l.put(path, processLOC(path));

		return l;
	}

	protected ChangedFile.Builder processChangeFile(final String path,
			final boolean attemptParse) {
		final ChangedFile.Builder fb = ChangedFile.newBuilder();
		fb.setName(path);
		fb.setKind(FileKind.OTHER);

		final String lowerPath = path.toLowerCase();
		if (lowerPath.endsWith(".txt"))
			fb.setKind(FileKind.TEXT);
		else if (lowerPath.endsWith(".xml"))
			fb.setKind(FileKind.XML);
		else if (lowerPath.endsWith(".jar") || lowerPath.endsWith(".class"))
			fb.setKind(FileKind.BINARY);
		else if (lowerPath.endsWith(".java") && attemptParse) {
			final String content = getFileContents(path);

			fb.setKind(FileKind.SOURCE_JAVA_JLS2);
			if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_4,
					AST.JLS2, false, null, null)) {
				if (debug)
					System.err.println("Found JLS2 parse error in: revision "
							+ id + ": file " + path);

				fb.setKind(FileKind.SOURCE_JAVA_JLS3);
				if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_5,
						AST.JLS3, false, null, null)) {
					if (debug)
						System.err
								.println("Found JLS3 parse error in: revision "
										+ id + ": file " + path);

					fb.setKind(FileKind.SOURCE_JAVA_JLS4);
					if (!parseJavaFile(path, fb, content, JavaCore.VERSION_1_7,
							AST.JLS4, false, null, null)) {
						if (debug)
							System.err
									.println("Found JLS4 parse error in: revision "
											+ id + ": file " + path);

						// fb.setContent(content);
						fb.setKind(FileKind.SOURCE_JAVA_ERROR);
					} else if (debug)
						System.err.println("Accepted JLS4: revision " + id
								+ ": file " + path);
				} else if (debug)
					System.err.println("Accepted JLS3: revision " + id
							+ ": file " + path);
			} else if (debug)
				System.err.println("Accepted JLS2: revision " + id + ": file "
						+ path);
		}

		return fb;
	}

	private boolean parseJavaFile(final String path,
			final ChangedFile.Builder fb, final String content,
			final String compliance, final int astLevel,
			final boolean storeOnError, Writer astWriter, String key) {
		try {
			final ASTParser parser = ASTParser.newParser(astLevel);
			parser.setKind(ASTParser.K_COMPILATION_UNIT);
			parser.setResolveBindings(true);
			parser.setSource(content.toCharArray());
			final Map options = JavaCore.getOptions();
			JavaCore.setComplianceOptions(compliance, options);
			parser.setCompilerOptions(options);

			final CompilationUnit cu = (CompilationUnit) parser.createAST(null);

			final JavaErrorCheckVisitor errorCheck = new JavaErrorCheckVisitor();
			cu.accept(errorCheck);

			if (!errorCheck.hasError || storeOnError) {
				final ASTRoot.Builder ast = ASTRoot.newBuilder();
				// final CommentsRoot.Builder comments =
				// CommentsRoot.newBuilder();
				final JavaVisitor visitor = new JavaVisitor(content);
				try {
					ast.addNamespaces(visitor.getNamespaces(cu));
					for (final String s : visitor.getImports())
						ast.addImports(s);
					/*
					 * for (final Comment c : visitor.getComments())
					 * comments.addComments(c);
					 */
				} catch (final UnsupportedOperationException e) {
					return false;
				} catch (final Exception e) {
					if (debug)
						System.err.println("Error visiting: " + path);
					e.printStackTrace();
					return false;
				}

				if (astWriter != null) {
					try {
						astWriter.append(new Text(key), new BytesWritable(ast
								.build().toByteArray()));
					} catch (Exception e) {
						//e.printStackTrace();
					}
				} else
					fb.setAst(ast);
				// fb.setComments(comments);
			}

			return !errorCheck.hasError;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	private boolean parseJavaScriptFile(final String path,
			final ChangedFile.Builder fb, final String content,
			final String compliance, final int astLevel,
			final boolean storeOnError, Writer astWriter, String key) {
		try {
			//System.out.println("parsing=" + (++count) + "\t" + path);
			final org.eclipse.wst.jsdt.core.dom.ASTParser parser = org.eclipse.wst.jsdt.core.dom.ASTParser
					.newParser(astLevel);
			parser.setKind(ASTParser.K_COMPILATION_UNIT);
			parser.setResolveBindings(true);
			parser.setSource(content.toCharArray());

			final Map options = JavaCore.getOptions();
			JavaCore.setComplianceOptions(compliance, options);
			parser.setCompilerOptions(options);

			JavaScriptUnit cu;
			try{
				cu = (JavaScriptUnit) parser.createAST(null);
			}catch(java.lang.IllegalArgumentException ex){
				return false;
			}

			final JavaScriptErrorCheckVisitor errorCheck = new JavaScriptErrorCheckVisitor();
			cu.accept(errorCheck);

			if (!errorCheck.hasError || storeOnError) {
				final ASTRoot.Builder ast = ASTRoot.newBuilder();
				// final CommentsRoot.Builder comments =
				// CommentsRoot.newBuilder();
				final JavaScriptVisitor visitor = new JavaScriptVisitor(content);
				try {
					ast.addNamespaces(visitor.getNamespaces(cu));
					// for (final String s : visitor.getImports())
					// ast.addImports(s);
					/*
					 * for (final Comment c : visitor.getComments())
					 * comments.addComments(c);
					 */
				} catch (final UnsupportedOperationException e) {
					return false;
				} catch (final Exception e) {
					if (debug)
						System.err.println("Error visiting: " + path);
					//e.printStackTrace();
					return false;
				}

				if (astWriter != null) {
					try {
						astWriter.append(new Text(key), new BytesWritable(ast
								.build().toByteArray()));
					} catch (IOException e) {
						e.printStackTrace();
					}catch(java.lang.NullPointerException e){
						
					}
				} else
					fb.setAst(ast);
				// fb.setComments(comments);
			}

			return !errorCheck.hasError;
		} catch (final Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	protected String processLOC(final String path) {
		String loc = "";

		final String lowerPath = path.toLowerCase();
		if (!(lowerPath.endsWith(".txt") || lowerPath.endsWith(".xml") || lowerPath
				.endsWith(".java")))
			return loc;

		final String content = getFileContents(path);

		final File dir = new File(
				new File(System.getProperty("java.io.tmpdir")), UUID
						.randomUUID().toString());
		final File tmpPath = new File(dir, path.substring(0,
				path.lastIndexOf("/")));
		tmpPath.mkdirs();
		final File tmpFile = new File(tmpPath, path.substring(path
				.lastIndexOf("/") + 1));
		FileIO.writeFileContents(tmpFile, content);

		try {
			final Process proc = Runtime.getRuntime().exec(
					new String[] { "/home/boa/ohcount/bin/ohcount", "-i",
							tmpFile.getPath() });

			final BufferedReader outStream = new BufferedReader(
					new InputStreamReader(proc.getInputStream()));
			String line = null;
			while ((line = outStream.readLine()) != null)
				loc += line;
			outStream.close();

			proc.waitFor();
		} catch (final IOException e) {
			e.printStackTrace();
		} catch (final InterruptedException e) {
			e.printStackTrace();
		}

		try {
			FileIO.delete(dir);
		} catch (final IOException e) {
			e.printStackTrace();
		}

		return loc;
	}
}
