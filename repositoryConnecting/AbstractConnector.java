package repositoryConnecting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.hadoop.hbase.client.HTableInterface;
import org.apache.hadoop.hbase.client.HTablePool;
import org.apache.hadoop.io.SequenceFile.Writer;

import boa.types.Code.Revision;

/**
 * @author rdyer
 */
public abstract class AbstractConnector implements AutoCloseable {
	protected List<AbstractCommit> revisions = null;
	public static int count=0;
	public abstract String getLastCommitId();
	public abstract void setLastSeenCommitId(final String id);

	public List<Revision> getCommits(final boolean parse) {
		if (revisions == null) {
			revisions = new ArrayList<AbstractCommit>();
			setRevisions();
		}
		final List<Revision> revs = new ArrayList<Revision>();
		for (final AbstractCommit rev : revisions)
			revs.add(rev.asProtobuf(parse));

		return revs;
	}

	protected abstract void setRevisions();

	public abstract void getTags(final List<String> names, final List<String> commits);

	public abstract void getBranches(final List<String> names, final List<String> commits);

	protected Map<String, Integer> revisionMap;

	public List<Revision> getCommits(final boolean parse, final Writer astWriter, final String repoKey, final String keyDelim) {
		if (revisions == null) {
			revisions = new ArrayList<AbstractCommit>();
			setRevisions();
		}
		final List<Revision> revs = new ArrayList<Revision>();
		for (final AbstractCommit rev : revisions){
			revs.add(rev.asProtobuf(parse, astWriter, repoKey, keyDelim));
		}

		System.out.println("Number of Commits:"+revs.size());
		return revs;
	}

	public List<Revision> getCommits(final boolean parse, final HTablePool tablePool, final HTableInterface table, final byte[] tableName, final String repoKey, final String keyDelim) {
		if (revisions == null) {
			revisions = new ArrayList<AbstractCommit>();
			setRevisions();
		}
		final List<Revision> revs = new ArrayList<Revision>();
		for (final AbstractCommit rev : revisions)
			revs.add(rev.asProtobuf(parse, tablePool, table, tableName, repoKey, keyDelim));

		return revs;
	}
}
