package main;

import java.io.File;
import java.util.concurrent.*;

import org.eclipse.wst.jsdt.internal.ui.wizards.buildpaths.newsourcepage.LinkFolderDialog;
import org.gitective.core.filter.commit.BugFilter;

import sfnet.JSONProjectCacher;
import sfnet.JSONSVNProjectCacher;
import util.Properties;

/**
 * @author rdyer
 */
public class CacheSVNJSON {
	static File jsonDir = new File(Properties.getProperty("gh.json.path", main.DefaultProperties.SF_JSON_PATH));
	 static File jsonCacheDir = new File(Properties.getProperty("gh.json.cache.path", main.DefaultProperties.GH_JSON_CACHE_PATH));

		public static void main(String[] args) {
			convertMetadataInSeq(args);
		}
	 
	 
	public static void convertMetadataInSeq(String[] args) {
		final long startTime = System.currentTimeMillis();
		jsonDir=new File(args[0]);
		jsonCacheDir=new File(args[1]);
//		ExecutorService pool = Executors.newFixedThreadPool(Integer.parseInt(Properties.getProperty("num.threads", main.DefaultProperties.NUM_THREADS)));

		for (final File file : jsonDir.listFiles()) {
			if (!file.getName().endsWith(".json"))
				continue;
			new CacheTaskSeq(file,args[0],args[1],args[2],args[3],args[4],args[5]).run();
		}

//		pool.shutdown();
//		try {
//			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
//		} catch (InterruptedException e) { }

		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	public static class CacheTask implements Runnable {
		final File file;
		final String path;
		final String bugzillaPath;
		final String jiraPath;
		final String product;
		final String protocol;
		final String linkForCode;
		public CacheTask(final File file) {
			this.file = file;
			path="";
			bugzillaPath=null;
			jiraPath=null;
			product=null;
			protocol=null;
			linkForCode=null;
		}
		
		public CacheTask(final File file,String path,String bug,String jira,String prod,String prot,String link) {
			this.file = file;
			this.path=path;
			bugzillaPath=bug;
			jiraPath=jira;
			product=prod;
			protocol=prot;
			linkForCode=link;
		}

		@Override
		public void run() {
			JSONSVNProjectCacher.readJSONProject(file, new File(jsonCacheDir, file.getName().substring(0, file.getName().length() - 5)),this.path,bugzillaPath,jiraPath,product,protocol,linkForCode);
		}
	}
	
	
	
	public static class CacheTaskSeq{
		final File file;
		final String path;
		final String bugzillaPath;
		final String jiraPath;
		final String product;
		final String protocol;
		final String linkForCode;
		public CacheTaskSeq(final File file) {
			this.file = file;
			path="";
			bugzillaPath=null;
			jiraPath=null;
			product=null;
			protocol=null;
			linkForCode=null;
		}
		
		public CacheTaskSeq(final File file,String path,String bug,String jira,String prod,String prot,String link) {
			this.file = file;
			this.path=path;
			bugzillaPath=bug;
			jiraPath=jira;
			product=prod;
			protocol=prot;
			linkForCode=link;
		}

		
		public void run() {
			JSONSVNProjectCacher.readJSONProject(file, new File(jsonCacheDir, file.getName().substring(0, file.getName().length() - 5)),this.path,bugzillaPath,jiraPath,product,protocol,linkForCode);
		}
	}
}
