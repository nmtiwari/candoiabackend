package main;

import java.io.*;
import java.util.HashMap;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.*;

import com.google.protobuf.InvalidProtocolBufferException;

import sfnet.JSONProjectCacher;
import boa.types.Toplevel.Project;
import util.FileIO;
import util.Properties;

/**
 * @author rdyer
 */
public class HBaseProjectImporter {
	final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);

	final static String keyDelim = Properties.getProperty("hbase.delimiter", main.DefaultProperties.HBASE_DELIMITER);

	final static byte[] meta_family = Bytes.toBytes(Properties.getProperty("hbase.projects.col", main.DefaultProperties.HBASE_PROJECTS_COL));
	final static byte[] meta_qualifier = Bytes.toBytes("proto");

	final static byte[] tableName = Bytes.toBytes(Properties.getProperty("hbase.projects.table", main.DefaultProperties.HBASE_PROJECTS_TABLE));
	final static HTablePool tablePool = new HTablePool();

	private final static File gitRoot = new File(Properties.getProperty("gh.svn.path", main.DefaultProperties.GH_GIT_PATH));

	private final static File jsonDir = new File(Properties.getProperty("gh.json.path", main.DefaultProperties.GH_JSON_PATH));
	private final static File jsonCacheDir = new File(Properties.getProperty("gh.json.cache.path", main.DefaultProperties.GH_JSON_CACHE_PATH));
	
	private final static HashMap<String, Project> cacheOfProjects = new HashMap<>();

	public static void main(String[] args) {
		final long startTime = System.currentTimeMillis();
		
		buildCacheOfProjects();

		ExecutorService pool = Executors.newFixedThreadPool(Integer.parseInt(Properties.getProperty("num.threads", main.DefaultProperties.NUM_THREADS)));
		
		System.out.println("Processing metadata of " + cacheOfProjects.size() + " projects.");
		for (String id : cacheOfProjects.keySet()) {
			Project p = cacheOfProjects.get(id);
			/*if (debug)
				print(id, p);*/
			pool.submit(new ImportTask(p));
		}

		pool.shutdown();
		try {
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) { }

		try {
			tablePool.close();
		} catch (IOException e) { }

		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	private static void print(String id, Project p) {
		System.out.print(id);
		System.out.print(" " + p.getId());
		System.out.print(" " + p.getName());
		System.out.print(" " + p.getHomepageUrl());
		if (p.getProgrammingLanguagesCount() > 0) {
			System.out.print(" Programming languages:" + p.getProgrammingLanguagesCount());
			for (int i = 0; i < p.getProgrammingLanguagesCount(); i++)
				System.out.print(" " + p.getProgrammingLanguages(i));
		}
		System.out.println();
	}

	private static void buildCacheOfProjects() {
		for (File file : jsonCacheDir.listFiles()) {
			if (file.getName().endsWith("-buf-map")) {
				HashMap<String, byte[]> repos = (HashMap<String, byte[]>) FileIO.readObjectFromFile(file.getAbsolutePath());
				for (String key : repos.keySet()) {
					Project p = null;
					try {
						p = Project.parseFrom(repos.get(key));
					} catch (InvalidProtocolBufferException e) {
						if (debug)
							e.printStackTrace();
						else
							System.err.println(e.getMessage());
					}
					cacheOfProjects.put(p.getId(), p);
				}
			}
		}
	}

	public static class ImportTask implements Runnable {
		final Project project;
		HTableInterface table;

		public ImportTask(final Project p) {
			this.project = p;
		}

		private void getTable() {
			if (table != null) {
				closeTable();
				try {
					Thread.sleep(1000);
				} catch (Exception e) {}
			}

			table = tablePool.getTable(tableName);

			// allow Puts to buffer to improve import speed
			table.setAutoFlush(false, true);
			try {
				table.setWriteBufferSize(12*1024*1024);
			} catch (final IOException e) {
				printError(e, "error setting write buffer");
			}
		}

		private void closeTable() {
			try {
				table.close();
			} catch (final IOException e) {
				printError(e, "error closing table");
			}

			table = null;
		}

		@Override
		public void run() {
			System.out.println("Importing " + project.getId() + " " + project.getName());
			try {
				getTable();

				// store the project metadata
				final Put put = new Put(Bytes.toBytes("g:" + project.getId()));
				put.setWriteToWAL(false); // dont write to WAL to speedup import
				put.add(meta_family, meta_qualifier, project.toByteArray());

				storePut(put);

				closeTable();
			} catch (final Exception e) {
				printError(e, "unknown error");
			}
		}

		private void storePut(final Put put) {
			while (true) {
				try {
					table.put(put);
					break;
				} catch (IllegalArgumentException e) {
					// probably tried inserting an empty put, ignore it
					break;
				} catch (final IOException e) {
					printError(e, "error writing table row");
					getTable();
				}
			}
		}
	}

	private static void printError(final Exception e, final String message) {
		System.err.println("ERR: " + message);
		if (debug) {
			e.printStackTrace();
			//System.exit(-1);
		}
		else
			System.err.println(e.getMessage());
	}
}
