package main;

import java.io.IOException;
import java.util.HashMap;
import java.util.Random;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import com.google.protobuf.CodedInputStream;

import boa.types.Ast.ASTRoot;
import boa.types.Toplevel.Project;
import util.Properties;

public class TestSorting {
	private final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);
	private static Configuration conf = null;
	private static FileSystem fileSystem = null;

	public static void main(String[] args) throws IOException {
		conf = new Configuration();
		conf.set("fs.default.name", "hdfs://boa-nn1/");
		fileSystem = FileSystem.get(conf);
		
		Random rand = new Random();
		int numOfUnsorted = 0, numOfSamples = 0;
		HashMap<String, Project> values = new HashMap<>();
		SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, new Path("hdfs://boa-nn1/tmprepcache/2015-07/projects-boa-8-5-1437665345.seq"), conf);
		final Text key = new Text(), preKey = new Text();
		final BytesWritable value = new BytesWritable();
		while (r.next(key, value)) {
			numOfUnsorted++;
			int i = rand.nextInt(10);
			if (i == 0) {
				System.out.println(numOfUnsorted + ": " + value.getLength());
				numOfSamples++;
				values.put(key.toString(), Project.parseFrom(CodedInputStream.newInstance(value.getBytes(), 0, value.getLength())));
			}
			else
				values.put(key.toString(), null);
		}
		r.close();
		
		System.out.println(numOfUnsorted + ": " + numOfSamples);
		
		int numOfSorted = 0;
		r = new SequenceFile.Reader(fileSystem, new Path("hdfs://boa-nn1/repcache/2015-07/projects-boa-8-5.seq/part-00000"), conf);
		boolean isSorted = true;
		int keyDiffs = 0, valueDiffs = 0, byteDiffs = 0;
		while (r.next(key, value)) {
			numOfSorted++;
			if (isSorted && key.compareTo(preKey) < 0)
				isSorted = false;
			String id = key.toString();
			if (values.containsKey(id)) {
				Project p = values.get(id);
				if (p != null) {
					numOfSamples--;
					Project p2 = Project.parseFrom(CodedInputStream.newInstance(value.getBytes(), 0, value.getLength()));
					if (!equals(p, p2)) {
						valueDiffs++;
						System.out.println(p.getName() + " - " + p2.getName());
					}
				}
			}
			else {
				keyDiffs++;
			}
		}
		r.close();

		System.out.println("Sorted: " + isSorted);
		System.out.println("Key diffs: " + keyDiffs);
		System.out.println("Value diffs: " + valueDiffs);
		System.out.println("Samples: " + numOfSamples);
		System.out.println("Unsorted - Sorted: " + numOfUnsorted + " - " + numOfSorted);
	}

	private static boolean equals(Project p1, Project p2) {
		return p1.equals(p2);
		//return p1.getId().equals(p2.getId());
	}

	private static boolean equals(byte[] bytes1, byte[] bytes2) {
		if (bytes1.length != bytes2.length) return false;
		for (int i = 0; i < bytes1.length; i++)
			if (bytes1[i] != bytes2[i])
				return false;
		return true;
	}

}
