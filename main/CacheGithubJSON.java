package main;

import java.io.File;
import java.util.concurrent.*;

import sfnet.JSONProjectCacher;

import util.Properties;

/**
 * @author rdyer
 */
public class CacheGithubJSON {
	static File jsonDir = new File(Properties.getProperty("gh.json.path", main.DefaultProperties.SF_JSON_PATH));
	 static File jsonCacheDir = new File(Properties.getProperty("gh.json.cache.path", main.DefaultProperties.GH_JSON_CACHE_PATH));

		public static void main(String[] args) {
			convertMetadataInSeq(args);
		}
	 
	 
	public static void convertMetadataInSeq(String[] args) {
		final long startTime = System.currentTimeMillis();
		jsonDir=new File(args[0]);
		jsonCacheDir=new File(args[1]);

		for (final File file : jsonDir.listFiles()) {
			if (!file.getName().endsWith(".json"))
				continue;
			System.out.println("jira in caonvert:"+args[3]);
				new CacheTaskSeq(file,args[1],args[2],args[3],args[4]).run();
		}
		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	public static class CacheTask implements Runnable {
		final File file;
		final String path;
		final String bugzillaPath;
		final String jiraPath;
		final String product;
		
		public CacheTask(final File file) {
			this.file = file;
			path="";
			bugzillaPath=null;
			jiraPath=null;
			product=null;
		}
		
		public CacheTask(final File file,String path,String bug,String jira,String prod) {
			this.file = file;
			this.path=path;
			path="";
			bugzillaPath=bug;
			jiraPath=jira;
			product =prod;
		}

		@Override
		public void run() {
			JSONProjectCacher.readJSONProject(file, new File(jsonCacheDir, file.getName().substring(0, file.getName().length() - 5)),this.path,bugzillaPath,jiraPath,product);
		}
	}
	
	
	
	public static class CacheTaskSeq{
		final File file;
		final String path;
		final String bugzillaPath;
		final String jiraPath;
		final String product;
		public CacheTaskSeq(final File file) {
			this.file = file;
			path="";
			bugzillaPath=null;
			jiraPath=null;
			product=null;
		}
		
		public CacheTaskSeq(final File file,String path,String bug,String jira,String prod) {
			this.file = file;
			this.path=path;
			bugzillaPath=bug;
			jiraPath=jira;
			product=prod;
		}

		
		public void run() {
			JSONProjectCacher.readJSONProject(file, new File(jsonCacheDir, file.getName().substring(0, file.getName().length() - 5)),this.path,bugzillaPath,jiraPath,product);
		}
	}
}
