package main;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.tmatesoft.svn.core.SVNException;
import org.tmatesoft.svn.core.SVNURL;
import org.tmatesoft.svn.core.wc.SVNWCUtil;

import repositoryConnecting.AbstractConnector;
import repositoryConnecting.GitConnector;
import repositoryConnecting.SVNConnector;
import boa.types.Code.CodeRepository;
import boa.types.Code.CodeRepository.RepositoryKind;
import boa.types.Code.Revision;
import boa.types.Diff.ChangedFile;
import boa.types.Toplevel.Project;
import boa.types.Toplevel.Project.ForgeKind;

public class SeqGenLocalSVN{
    private final static String keyDelim = "!!";

    private static SequenceFile.Writer projectWriter, astWriter;
    private static Configuration conf = null;
    private static FileSystem fileSystem = null;

    public static void main(String[] args) throws IOException, InterruptedException {
        conf = new Configuration();
        fileSystem = FileSystem.get(conf);

        openWriters(args[1]);
        //storeRepo(args[0]);
        storeRepo("file:///"+args[0]);
        closeWriters();
        try {
            MapFileGen.main(args);
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        //deleteAstFile(args);
    }

    public static void openWriters(String repositoryPath) {
        while (true) {
            try {
                projectWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/projects-small.seq"), Text.class, BytesWritable.class);
                astWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/ast"), Text.class, BytesWritable.class);
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }

    public static void closeWriters() {
        while (true) {
            try {
                projectWriter.close();
                astWriter.close();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }

    public static void deleteAstFile(String[] args) {
        try{
            File file = new File(args[1]+"/ast");
            file.delete();

        }catch(Exception e){

            e.printStackTrace();

        }
    }

    private static void storeRepo(String path) throws IOException {
        final Project.Builder projBuilder = Project.newBuilder();
        final File gitDir = new File(path);
        projBuilder.setName(gitDir.getName());
        projBuilder.setId(path);
        projBuilder.setProjectUrl(path);
        projBuilder.setKind(ForgeKind.OTHER);
        try (final AbstractConnector conn = new SVNConnector(path,"","")) {
            final CodeRepository.Builder repoBuilder = CodeRepository.newBuilder();
            repoBuilder.setUrl(path);
            repoBuilder.setKind(RepositoryKind.SVN);
            final String repoKey = "g:" + path + keyDelim + path;

            for (final Revision rev : conn.getCommits(true, astWriter, repoKey, keyDelim)) {
                final Revision.Builder revBuilder = Revision.newBuilder(rev);
                repoBuilder.addRevisions(revBuilder);
            }
            projBuilder.addCodeRepositories(repoBuilder);
        } catch (final Exception e) {
            e.printStackTrace();
        }
        Project project = projBuilder.build();
        projectWriter.append(new Text(project.getId()), new BytesWritable(project.toByteArray()));
    }
    
    
    
    private static void storeRepo(String path,Project.Builder projBuilder) throws IOException {
    	 final File gitDir = new File(path);
        try (final AbstractConnector conn = new SVNConnector(path,"","")) {
            final CodeRepository.Builder repoBuilder = CodeRepository.newBuilder();
            repoBuilder.setKind(RepositoryKind.SVN);
            final String repoKey = "g:" + path + keyDelim + path;

            for (final Revision rev : conn.getCommits(true, astWriter, repoKey, keyDelim)) {
                final Revision.Builder revBuilder = Revision.newBuilder(rev);
                repoBuilder.addRevisions(revBuilder);
            }
            projBuilder.addCodeRepositories(repoBuilder);
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
    
    public static void generate(String pathOfRepo,SequenceFile.Writer projWrite,SequenceFile.Writer astWrite ){
    	projectWriter=projWrite;
    	astWriter=astWrite;
    	 try {
			storeRepo("file:///"+pathOfRepo);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
    
    public static void generate(Project.Builder projBuilder,String pathOfRepo,SequenceFile.Writer astWrite ){
    	astWriter=astWrite;
    	 try {
			storeRepo(pathOfRepo,projBuilder);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }
}