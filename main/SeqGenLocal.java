package main;

import java.io.File;
import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import repositoryConnecting.AbstractConnector;
import repositoryConnecting.GitConnector;
import boa.types.Code.CodeRepository;
import boa.types.Code.CodeRepository.RepositoryKind;
import boa.types.Code.Revision;
import boa.types.Toplevel.Project;
import boa.types.Toplevel.Project.ForgeKind;

public class SeqGenLocal {
	private final static String keyDelim = "!!";
			
	private static SequenceFile.Writer projectWriter, astWriter;
	private static Configuration conf = null;
	private static FileSystem fileSystem = null;

	public static void main(String[] args) throws IOException, InterruptedException {
		conf = new Configuration();
		fileSystem = FileSystem.get(conf);
		openWriters();
		//storeRepo(args[0]);
		storeRepo("F:/github/Aph0/HeadTilter");
		closeWriters();
	}

	public static void openWriters() {
		while (true) {
			try {
				projectWriter = SequenceFile.createWriter(fileSystem, conf, new Path("F:/github/projects"), Text.class, BytesWritable.class);
				astWriter = SequenceFile.createWriter(fileSystem, conf, new Path("F:/github/ast"), Text.class, BytesWritable.class);
				break;
			} catch (Throwable t) {
				t.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
	}

	public static void closeWriters() {
		while (true) {
			try {
				projectWriter.close();
				astWriter.close();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				break;
			} catch (Throwable t) {
				t.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
	}
	
	private static void storeRepo(String path) throws IOException {
		final Project.Builder projBuilder = Project.newBuilder();
		final File gitDir = new File(path);
		
		projBuilder.setName(gitDir.getName());
		projBuilder.setId(path);
		projBuilder.setProjectUrl(path);
		projBuilder.setKind(ForgeKind.OTHER);
		
		try (final AbstractConnector conn = new GitConnector(gitDir.getAbsolutePath())) {
			final CodeRepository.Builder repoBuilder = CodeRepository.newBuilder();
			repoBuilder.setUrl(path);
			repoBuilder.setKind(RepositoryKind.GIT);
			final String repoKey = "g:" + path + keyDelim + path;
			for (final Revision rev : conn.getCommits(true, astWriter, repoKey, keyDelim)) {
				final Revision.Builder revBuilder = Revision.newBuilder(rev);
				repoBuilder.addRevisions(revBuilder);
			}

			projBuilder.addCodeRepositories(repoBuilder);
		} catch (final Exception e) {
			e.printStackTrace();
		}
		Project project = projBuilder.build();
		projectWriter.append(new Text(project.getId()), new BytesWritable(project.toByteArray()));
	}
}
