package main;

import java.io.File;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Scanner;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import com.google.protobuf.CodedInputStream;

import boa.types.Ast.ASTRoot;
import repositoryConnecting.GitConnector;
import util.FileIO;
import util.Properties;

public class TestGitConnector {
	private static final HashMap<String, String[]> repoInfo = new HashMap<String, String[]>();
	private final static File gitRootPath = new File(Properties.getProperty("gh.svn.path", main.DefaultProperties.GH_GIT_PATH));
	

	private static SequenceFile.Writer astWriter;
	
	private static Configuration conf = null;
	private static FileSystem fileSystem = null;

	public static void main(String[] args) throws IOException {
		conf = new Configuration();
		fileSystem = FileSystem.get(conf);
		
		String path = "F:/github/Aph0/HeadTilter";
		//String path = "/hadoop4/github/repositories/204/SnippyHolloW/RestoPhoto";
		//String path = "F:/github/rydnr/java-commons";
		//String path = "F:/github/junit-team/junit";
		//String path = "F:/github/JetBrains/intellij-community";
		long start = System.currentTimeMillis();
		openWriters("Aph0#HeadTilter");
		GitConnector gc = new GitConnector(path);
		try {
			gc.getCommits(true, astWriter, path, "!!");
		} catch (Exception e) {
			System.err.println(e.getMessage());
		}
		gc.close();
		closeWriters();
		
		SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, new Path("Aph0#HeadTilter-ast.seq"), conf);
		final Text key = new Text();
		final BytesWritable val = new BytesWritable();
		while (r.next(key, val)) {
			System.out.println(key);
			System.out.println(ASTRoot.parseFrom(CodedInputStream.newInstance(val.getBytes(), 0, val.getLength())));
		}
		r.close();
		
		long end = System.currentTimeMillis();
		System.out.println("Time: " + (end - start) / 1000);
	}
	
	private static void openWriters(String name) {
		while (true) {
			try {
				astWriter = SequenceFile.createWriter(fileSystem, conf, new Path(name + "-ast.seq"), Text.class, BytesWritable.class);
				break;
			} catch (Throwable t) {
				t.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
	}

	private static void closeWriters() {
		while (true) {
			try {
				astWriter.close();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
				break;
			} catch (Throwable t) {
				t.printStackTrace();
				try {
					Thread.sleep(1000);
				} catch (InterruptedException e) {}
			}
		}
	}

	private static void getRepoInfo() {
		String content = FileIO.readFileContents(new File("repos-Java-org-commits.csv"));
		Scanner sc = new Scanner(content);
		while (sc.hasNextLine()) {
			String[] parts = sc.nextLine().split(",");
			repoInfo.put(parts[0], new String[]{parts[1], parts[3]});
		}
		sc.close();
	}

	private static File exists(String name, String listId) {
		for (int i = 2; i <= 4; i++) {
			File dir = new File("/hadoop" + i + "/" + gitRootPath + "/" + listId + "/" + name);
			if (dir.exists())
				return dir;
		}
		return null;
	}

}
