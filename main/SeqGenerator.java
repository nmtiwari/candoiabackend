package main;

import java.io.*;
import java.util.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.*;
import org.apache.hadoop.io.*;
import com.google.protobuf.CodedInputStream;

import boa.types.Toplevel.Project;
import util.Properties;

/**
 * @author rdyer
 */
public class SeqGenerator {
	final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);

	final static byte[] meta_family = Bytes.toBytes(Properties.getProperty("hbase.projects.col", main.DefaultProperties.HBASE_PROJECTS_COL));
	final static byte[] meta_qualifier = Bytes.toBytes("proto");
	final static byte[] ast_family = Bytes.toBytes(Properties.getProperty("hbase.ast.col", main.DefaultProperties.HBASE_AST_COL));
	final static byte[] comments_family = Bytes.toBytes(Properties.getProperty("hbase.comments.col", main.DefaultProperties.HBASE_COMMENTS_COL));
	final static byte[] issues_family = Bytes.toBytes(Properties.getProperty("hbase.issues.col", main.DefaultProperties.HBASE_ISSUES_COL));

	final static byte[] projTableName = Bytes.toBytes(Properties.getProperty("hbase.projects.table", main.DefaultProperties.HBASE_PROJECTS_TABLE));
	final static byte[] astTableName = Bytes.toBytes(Properties.getProperty("hbase.ast.table", main.DefaultProperties.HBASE_AST_TABLE));
	final static byte[] commentsTableName = Bytes.toBytes(Properties.getProperty("hbase.comments.table", main.DefaultProperties.HBASE_COMMENTS_TABLE));
	final static byte[] issuesTableName = Bytes.toBytes(Properties.getProperty("hbase.issues.table", main.DefaultProperties.HBASE_ISSUES_TABLE));

	final static String keyDelim = Properties.getProperty("hbase.delimiter", main.DefaultProperties.HBASE_DELIMITER);

	final static Path projPath = new Path(Properties.getProperty("seq.projects.path", main.DefaultProperties.SEQ_PROJECTS_PATH));
	final static Path astDir = new Path(Properties.getProperty("seq.ast.dir", main.DefaultProperties.SEQ_AST_DIR));
	final static Path astPath = new Path(astDir, Properties.getProperty("seq.ast.path", main.DefaultProperties.SEQ_AST_PATH));
	final static Path commentsDir = new Path(Properties.getProperty("seq.comments.dir", main.DefaultProperties.SEQ_COMMENTS_DIR));
	final static Path commentsPath = new Path(commentsDir, Properties.getProperty("seq.comments.path", main.DefaultProperties.SEQ_COMMENTS_PATH));
	final static Path issuesDir = new Path(Properties.getProperty("seq.issues.dir", main.DefaultProperties.SEQ_ISSUES_DIR));
	final static Path issuesPath = new Path(issuesDir, Properties.getProperty("seq.issues.path", main.DefaultProperties.SEQ_ISSUES_PATH));

	public static void main(final String[] args) throws Exception {
		if (args.length == 0) {
			System.out.println("Error: argument required");
			showHelp();
		}

		final long startTime = System.currentTimeMillis();
		for (int i = 0; i < args.length; i++)
			if ("-k".equals(args[i]) || "--cutoff".equals(args[i]))
				cutoff = new Integer(args[++i]);
			else if ("-s".equals(args[i]) || "--small".equals(args[i]))
				generateProjectsSmall();
			else if ("-c".equals(args[i]) || "--comments".equals(args[i]))
				generateComments();
			else if ("-p".equals(args[i]) || "--projects".equals(args[i]))
				generateProjects();
			else if ("-a".equals(args[i]) || "--ast".equals(args[i]))
				generateAST();
			else if ("-i".equals(args[i]) || "--issues".equals(args[i]) ||
					"-l".equals(args[i]) || "--loc".equals(args[i]))
				generateIssues();
			else {
				System.out.println("Invalid argument '" + args[i] + "'");
				showHelp();
			}
		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	private static int cutoff = 1;
	private static Random rand = new Random();

	private static void showHelp() {
		System.out.println("Usage: SeqGenerator [commands]");
		System.out.println("");
		System.out.println("Commands:");
		System.out.println("	-p, --projects");
		System.out.println("	-s, --small projects dataset");
		System.out.println("		-k, --cutoff size");
		System.out.println("	-a, --ast");
		System.out.println("	-c, --comments");
		System.out.println("	-i, --issues");
		System.out.println("	-l, --loc");
		System.exit(-1);
	}

	private static void generateProjects() throws IOException {
		final Configuration conf = HBaseConfiguration.create();

		final FileSystem fileSystem = FileSystem.get(conf);
		fileSystem.createNewFile(projPath);
		final SequenceFile.Writer w = SequenceFile.createWriter(fileSystem, conf, projPath, Text.class, BytesWritable.class);
		//final SequenceFile.Writer w = SequenceFile.createWriter(fileSystem, conf, projPath, Text.class, BytesWritable.class, CompressionType.BLOCK, new SnappyCodec());

		final HTable projTable = new HTable(conf, projTableName);
		final Scan scan = new Scan();
		scan.setCaching(200);
		scan.setFilter(new RowFilter(CompareFilter.CompareOp.NOT_EQUAL, new SubstringComparator(keyDelim)));
		scan.addColumn(meta_family, meta_qualifier);

		for (final Result r : projTable.getScanner(scan))
			try {
				if (debug)
					System.out.println("converting project for row: " + Bytes.toString(r.getRow()));

				final byte[] value = r.getValue(meta_family, meta_qualifier);
				final CodedInputStream inputStream = CodedInputStream.newInstance(value, 0, value.length);
				final Project project = Project.parseFrom(inputStream);
				w.append(new Text(project.getId()), new BytesWritable(project.toByteArray()));
			} catch (final Exception e) {
				if (debug) {
					System.err.println("error with reading row: " + Bytes.toString(r.getRow()));
					e.printStackTrace();
				}
			}
		projTable.close();
		w.close();
	}

	private static void generateProjectsSmall() throws Exception {
		final Configuration conf2 = new Configuration();
		conf2.set("fs.default.name", "hdfs://boa-nn1/");
		final FileSystem fileSystem2 = FileSystem.get(conf2);

		final SequenceFile.Reader r = new SequenceFile.Reader(fileSystem2, new Path("hdfs://boa-nn1/repcache/2013-05/projects.seq"), conf2);

		final Configuration conf = new Configuration();
		final FileSystem fileSystem = FileSystem.get(conf);

		final Text key = new Text();
		final BytesWritable val = new BytesWritable();

		final SequenceFile.Writer w = SequenceFile.createWriter(fileSystem, conf, projPath, Text.class, BytesWritable.class);
		//final SequenceFile.Writer w = SequenceFile.createWriter(fileSystem, conf, path, Text.class, BytesWritable.class, CompressionType.BLOCK, new SnappyCodec());
		long total = 0;
		long used = 0;

		while (r.next(key, val)) {
			total++;
			try {
				if (rand.nextInt(100) >= cutoff)
					continue;
				used++;

				if (debug)
					System.out.println("storing key: " + key);

				w.append(key, val);
			} catch (final Exception e) {
				if (debug) {
					System.err.println("error with reading key: " + key);
					e.printStackTrace();
				}
			}
		}

		System.out.println("number of projects in input:  " + total);
		System.out.println("number of projects in output: " + used);
		System.out.println("percentage:                   " + (used / (double)total * 100.0) + "%");

		r.close();
		w.close();
	}

	private static void generateAST() throws Exception {
		final Configuration conf = HBaseConfiguration.create();
		final HTable astTable = new HTable(conf, astTableName);
		final Comparator<KeyValue> comparator = new Comparator<KeyValue>() {
			@Override
			public int compare(KeyValue kv1, KeyValue kv2) {
				final byte[] kv1q = kv1.getQualifier();
				final byte[] kv2q = kv2.getQualifier();

				return Bytes.BYTES_COMPARATOR.compare(kv1q, kv2q);
			}
		};

		final FileSystem fileSystem = FileSystem.get(conf);
		fileSystem.createNewFile(astPath);

		final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, astDir.toString(), Text.class, BytesWritable.class);
		//final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, astDir.toString(), Text.class, BytesWritable.class, CompressionType.RECORD, new SnappyCodec(), null);
		//w.setIndexInterval(1);

		final Scan scan = new Scan();
		scan.setCaching(50);
		scan.addFamily(ast_family);

		for (final Result r : astTable.getScanner(scan))
			try {
				if (debug)
					System.out.println("converting ast for row: " + Bytes.toString(r.getRow()));

				final List<KeyValue> cols = Arrays.asList(r.raw());
				Collections.sort(cols, comparator);

				for (final KeyValue kv : cols)
					w.append(new Text(Bytes.toString(kv.getRow()) + keyDelim + Bytes.toString(kv.getQualifier())), new BytesWritable(kv.getValue()));
			} catch (final Exception e) {
				if (debug) {
					System.err.println("error with reading row: " + Bytes.toString(r.getRow()));
					e.printStackTrace();
				}
			}

		w.close();
		astTable.close();
	}

	private static void generateComments() throws Exception {
		final Configuration conf = HBaseConfiguration.create();
		final HTable commentsTable = new HTable(conf, commentsTableName);
		final Comparator<KeyValue> comparator = new Comparator<KeyValue>() {
			@Override
			public int compare(KeyValue kv1, KeyValue kv2) {
				final byte[] kv1q = kv1.getQualifier();
				final byte[] kv2q = kv2.getQualifier();

				return Bytes.BYTES_COMPARATOR.compare(kv1q, kv2q);
			}
		};

		final FileSystem fileSystem = FileSystem.get(conf);
		fileSystem.createNewFile(commentsPath);

		final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, commentsDir.toString(), Text.class, BytesWritable.class);
		//final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, commentsDir.toString(), Text.class, BytesWritable.class, CompressionType.RECORD, new SnappyCodec(), null);
		//w.setIndexInterval(1);

		final Scan scan = new Scan();
		scan.setCaching(50);
		scan.addFamily(comments_family);

		for (final Result r : commentsTable.getScanner(scan))
			try {
				if (debug)
					System.out.println("converting comments for row: " + Bytes.toString(r.getRow()));

				final List<KeyValue> cols = Arrays.asList(r.raw());
				Collections.sort(cols, comparator);

				for (final KeyValue kv : cols)
					w.append(new Text(Bytes.toString(kv.getRow()) + keyDelim + Bytes.toString(kv.getQualifier())), new BytesWritable(kv.getValue()));
			} catch (final Exception e) {
				if (debug) {
					System.err.println("error with reading row: " + Bytes.toString(r.getRow()));
					e.printStackTrace();
				}
			}

		w.close();
		commentsTable.close();
	}

	private static void generateIssues() throws Exception {
		final Configuration conf = HBaseConfiguration.create();
		final HTable issuesTable = new HTable(conf, issuesTableName);
		final Comparator<KeyValue> comparator = new Comparator<KeyValue>() {
			@Override
			public int compare(KeyValue kv1, KeyValue kv2) {
				final byte[] kv1q = kv1.getQualifier();
				final byte[] kv2q = kv2.getQualifier();

				return Bytes.BYTES_COMPARATOR.compare(kv1q, kv2q);
			}
		};

		final FileSystem fileSystem = FileSystem.get(conf);
		fileSystem.createNewFile(issuesPath);

		final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, issuesDir.toString(), Text.class, BytesWritable.class);
		//final MapFile.Writer w = new MapFile.Writer(conf, fileSystem, issuesDir.toString(), Text.class, BytesWritable.class, CompressionType.RECORD, new SnappyCodec(), null);
		//w.setIndexInterval(1);

		final Scan scan = new Scan();
		scan.setCaching(50);
		scan.addFamily(issues_family);

		for (final Result r : issuesTable.getScanner(scan))
			try {
				if (debug)
					System.out.println("converting issues for row: " + Bytes.toString(r.getRow()));

				final List<KeyValue> cols = Arrays.asList(r.raw());
				Collections.sort(cols, comparator);

				for (final KeyValue kv : cols)
					w.append(new Text(Bytes.toString(kv.getRow()) + keyDelim + Bytes.toString(kv.getQualifier())), new BytesWritable(kv.getValue()));
			} catch (final Exception e) {
				if (debug) {
					System.err.println("error with reading row: " + Bytes.toString(r.getRow()));
					e.printStackTrace();
				}
			}

		w.close();
		issuesTable.close();
	}
}
