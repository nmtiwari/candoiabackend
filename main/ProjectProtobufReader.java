package main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import com.google.protobuf.InvalidProtocolBufferException;

import boa.types.Toplevel.Project;
import util.FileIO;
import util.Properties;

public class ProjectProtobufReader {
	final static File jsonCacheDir = new File(Properties.getProperty("gh.json.cache.path", main.DefaultProperties.GH_JSON_CACHE_PATH));

	public static void main(String[] args) throws FileNotFoundException, IOException {
		//readSingleFile();
		readMap();
	}

	private static void readMap() throws InvalidProtocolBufferException {
		String path = "/remote/rs/tien/github/", outPath = path + "repos-metadata-Boa-upto1213";
		HashMap<String, byte[]> repos = (HashMap<String, byte[]>) FileIO.readObjectFromFile(outPath + "-0-buf-map");
		for (String key : repos.keySet()) {
			Project p = Project.newBuilder().mergeFrom(repos.get(key)).build();
			System.out.print(key);
			System.out.print(" " + p.getId());
			System.out.print(" " + p.getName());
			System.out.print(" " + p.getHomepageUrl());
			if (p.getProgrammingLanguagesCount() > 0) {
				System.out.print(" Programming languages:" + p.getProgrammingLanguagesCount());
				for (int i = 0; i < p.getProgrammingLanguagesCount(); i++)
					System.out.print(" " + p.getProgrammingLanguages(i));
			}
			System.out.println();
		}
	}

	private static void readSingleFile() throws IOException {
		for (File file : jsonCacheDir.listFiles()) {
			System.out.println(file.getName());
			Project p = Project.newBuilder().mergeFrom(new FileInputStream(file)).build();
			System.out.print(p.getId());
			System.out.print(" " + p.getName());
			System.out.print(" " + p.getHomepageUrl());
			if (p.getProgrammingLanguagesCount() > 0) {
				System.out.print(" Programming languages:" + p.getProgrammingLanguagesCount());
				for (int i = 0; i < p.getProgrammingLanguagesCount(); i++)
					System.out.print(" " + p.getProgrammingLanguages(i));
			}
			System.out.println();
		}
	}

}
