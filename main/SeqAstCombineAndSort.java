package main;

import java.io.EOFException;
import java.io.IOException;
import java.net.InetAddress;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import util.Properties;

public class SeqAstCombineAndSort {
	private final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);
	private static Configuration conf = null;
	private static FileSystem fileSystem = null;

	public static void main(String[] args) throws IOException {
		conf = new Configuration();
		conf.set("fs.default.name", "hdfs://boa-nn1/");
		fileSystem = FileSystem.get(conf);
		int numOfCombined = 0;
		if (!fileSystem.exists(new Path("hdfs://boa-nn1/repcache/2015-07/ast-combined.seq"))) {
			SequenceFile.Writer writer = SequenceFile.createWriter(fileSystem, conf, new Path("hdfs://boa-nn1/repcache/2015-07/ast-combined.seq"), Text.class, BytesWritable.class);
			FileStatus[] files = fileSystem.listStatus(new Path("hdfs://boa-nn1/tmprepcache/2015-07"));
			for (int i = 0; i < files.length; i++) {
				FileStatus file = files[i];
				String prefix = "ast-";
				String name = file.getPath().getName();
				int index1 = name.indexOf(prefix);
				if (index1 > -1) {
					System.out.println(name);
					try {
						SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, file.getPath(), conf);
						final Text key = new Text();
						final BytesWritable val = new BytesWritable();
						while (r.next(key)) {
							writer.append(key, val);
							numOfCombined++;
						}
						r.close();
					} catch (EOFException e) {
						printError(e, "EOF Exception in " + file.getPath().getName());
					}
				}
			}
			writer.close();
		}
		else {
			SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, new Path("hdfs://boa-nn1/repcache/2015-07/ast-combined.seq"), conf);
			final Text key = new Text();
			while (r.next(key)) {
				numOfCombined++;
			}
			r.close();
		}
		System.out.println("Combined: " + numOfCombined);
		
		SequenceFile.Sorter sorter = new SequenceFile.Sorter(fileSystem, Text.class, BytesWritable.class, conf);
		sorter.sort(new Path[]{new Path("hdfs://boa-nn1/repcache/2015-07/ast-combined.seq")}, new Path("hdfs://boa-nn1/repcache/2015-07/ast-sorted.seq"), false);
		
		int numOfSorted = 0;
		SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, new Path("hdfs://boa-nn1/repcache/2015-07/ast-sorted.seq"), conf);
		final Text key = new Text(), preKey = new Text();
		boolean isSorted = true;
		while (r.next(key)) {
			numOfSorted++;
			if (isSorted && key.compareTo(preKey) < 0)
				isSorted = false;
		}
		r.close();
		System.out.println("Sorted: " + isSorted);
		System.out.println("Combined - Sorted: " + numOfCombined + " - " + numOfSorted);
	}

	private static void printError(final Throwable e, final String message) {
		System.err.println("ERR: " + message);
		if (debug) {
			e.printStackTrace();
			//System.exit(-1);
		}
		else
			System.err.println(e.getMessage());
	}

}
