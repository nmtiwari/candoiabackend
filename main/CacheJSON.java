package main;

import java.io.File;
import java.util.concurrent.*;

import sfnet.JSONProjectCacher;
import util.Properties;

/**
 * @author rdyer
 */
public class CacheJSON {
	final static File jsonDir = new File(Properties.getProperty("sf.json.path", main.DefaultProperties.SF_JSON_PATH));
	final static File jsonCacheDir = new File(Properties.getProperty("sf.json.cache.path", main.DefaultProperties.SF_JSON_CACHE_PATH));

	public static void main(String[] args) {
		final long startTime = System.currentTimeMillis();
		
		ExecutorService pool = Executors.newFixedThreadPool(Integer.parseInt(Properties.getProperty("num.threads", main.DefaultProperties.NUM_THREADS)));

		for (final File file : jsonDir.listFiles()) {
			if (!file.getName().endsWith(".json"))
				continue;

			pool.submit(new CacheTask(file));

		}

		pool.shutdown();
		try {
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) { }

		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	public static class CacheTask implements Runnable {
		final File file;
		final String path;
		final String bugzillaPath;
		final String jiraPath;
		final String product;
		public CacheTask(final File file) {
			this.file = file;
			path="";
			bugzillaPath=null;
			jiraPath=null;
			product=null;
		}
		
		public CacheTask(final File file,String path,String bug,String jira,String prod) {
			this.file = file;
			this.path=path;
			bugzillaPath=bug;
			jiraPath=jira;
			product=prod;
		}

		@Override
		public void run() {
			JSONProjectCacher.readJSONProject(file, new File(jsonCacheDir, file.getName().substring(0, file.getName().length() - 5)),this.path,bugzillaPath,jiraPath,product);
		}
	}
}
