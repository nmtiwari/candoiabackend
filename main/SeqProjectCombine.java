package main;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import com.google.protobuf.CodedInputStream;

import boa.types.Toplevel.Project;

public class SeqProjectCombine {

	public static void main(String[] args) throws IOException {
		Configuration conf = new Configuration();
		conf.set("fs.default.name", "hdfs://boa-njt/");
		FileSystem fileSystem = FileSystem.get(conf);
		String base = conf.get("fs.default.name", "");
		
		HashMap<String, String> sources = new HashMap<>();
		HashSet<String> marks = new HashSet<>();
		FileStatus[] files = fileSystem.listStatus(new Path(base + "tmprepcache/2015-07"));
		for (int i = 0; i < files.length; i++) {
			FileStatus file = files[i];
			String name = file.getPath().getName();
			if (name.startsWith("projects-") && name.endsWith(".seq")) {
				System.out.println("Reading file " + i + " in " + files.length + ": " + name);
				SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, file.getPath(), conf);
				final Text key = new Text();
				final BytesWritable value = new BytesWritable();
				try {
					while (r.next(key, value)) {
						String s = key.toString();
						if (marks.contains(s)) continue;
						Project p = Project.parseFrom(CodedInputStream.newInstance(value.getBytes(), 0, value.getLength()));
						if (p.getCodeRepositoriesCount() > 0 && p.getCodeRepositories(0).getRevisionsCount() > 0)
							marks.add(s);
						sources.put(s, name);
					}
				} catch (Exception e) {
					System.err.println(name);
					e.printStackTrace();
				}
				r.close();
			}
		}
		SequenceFile.Writer w = SequenceFile.createWriter(fileSystem, conf, new Path(base + "repcache/2015-07/projects.seq"), Text.class, BytesWritable.class);
		for (int i = 0; i < files.length; i++) {
			FileStatus file = files[i];
			String name = file.getPath().getName();
			if (name.startsWith("projects-") && name.endsWith(".seq")) {
				System.out.println("Reading file " + i + " in " + files.length + ": " + name);
				SequenceFile.Reader r = new SequenceFile.Reader(fileSystem, file.getPath(), conf);
				final Text key = new Text();
				final BytesWritable value = new BytesWritable();
				try {
					while (r.next(key, value)) {
						String s = key.toString();
						if (sources.get(s).equals(name))
							w.append(key, value);
					}
				} catch (Exception e) {
					System.err.println(name);
					e.printStackTrace();
				}
				r.close();
			}
		}
		w.close();
		
		fileSystem.close();
	}

}
