package main;

import java.io.*;
import java.util.concurrent.*;

import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.util.*;

import sfnet.JSONProjectCacher;

import boa.types.Toplevel.Project;

import util.Properties;

/**
 * @author rdyer
 */
public class HBaseSFProjectImporter {
	final static boolean debug = Properties.getBoolean("debug", main.DefaultProperties.DEBUG);

	final static String keyDelim = Properties.getProperty("hbase.delimiter", main.DefaultProperties.HBASE_DELIMITER);

	final static byte[] meta_family = Bytes.toBytes(Properties.getProperty("hbase.projects.col", main.DefaultProperties.HBASE_PROJECTS_COL));
	final static byte[] meta_qualifier = Bytes.toBytes("proto");

	final static byte[] tableName = Bytes.toBytes(Properties.getProperty("hbase.projects.table", main.DefaultProperties.HBASE_PROJECTS_TABLE));
	final static HTablePool tablePool = new HTablePool();

	final static File svnRoot = new File(Properties.getProperty("sf.svn.path", main.DefaultProperties.SF_SVN_PATH));

	final static File jsonDir = new File(Properties.getProperty("sf.json.path", main.DefaultProperties.SF_JSON_PATH));
	final static File jsonCacheDir = new File(Properties.getProperty("sf.json.cache.path", main.DefaultProperties.SF_JSON_CACHE_PATH));

	public static void main(String[] args) {
		final long startTime = System.currentTimeMillis();

		ExecutorService pool = Executors.newFixedThreadPool(Integer.parseInt(Properties.getProperty("num.threads", main.DefaultProperties.NUM_THREADS)));

		for (final File file : jsonDir.listFiles())
			if (file.getName().endsWith(".json.txt"))
				pool.submit(new ImportTask(file));

		pool.shutdown();
		try {
			pool.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
		} catch (InterruptedException e) { }

		try {
			tablePool.close();
		} catch (IOException e) { }

		System.out.println("Time: " + (System.currentTimeMillis() - startTime) / 1000);
	}

	public static class ImportTask implements Runnable {
		final File file;
		HTableInterface table;
		String path="";
		String bugzillaIssuePath="";
		String jiraIssuePath="";
		String product="";
		public ImportTask(final File file) {
			this.file = file;
		}

		private void getTable() {
			if (table != null) {
				closeTable();
				try {
					Thread.sleep(1000);
				} catch (Exception e) {}
			}

			table = tablePool.getTable(tableName);

			// allow Puts to buffer to improve import speed
			table.setAutoFlush(false, false);
			try {
				table.setWriteBufferSize(12*1024*1024);
			} catch (final IOException e) {
				printError(e, "error setting write buffer");
			}
		}

		private void closeTable() {
			try {
				table.flushCommits();
			} catch (final IOException e) {
				printError(e, "error flushing commits");
			}

			try {
				table.close();
			} catch (final IOException e) {
				printError(e, "error closing table");
			}

			table = null;
		}

		@Override
		public void run() {
			try {
				getTable();

				final String projId = file.getName().substring(0, file.getName().indexOf("."));

//				if (!exists(projId)) {
					if (debug)
						System.out.println("Processing: " + file.getName());

					Project project = JSONProjectCacher.readJSONProject(file, new File(jsonCacheDir, projId),path,bugzillaIssuePath,jiraIssuePath,product);

					if (project != null) {
						if (debug)
							System.out.println("Putting in table: " + project.getId());

						// store the project metadata
						final Put put = new Put(Bytes.toBytes("sf:" + project.getId()));
						put.setWriteToWAL(false); // dont write to WAL to speedup import
						put.add(meta_family, meta_qualifier, project.toByteArray());

						storePut(put);
					}
//				} else
//					if (debug)
//						System.out.println("Skipping: " + file.getName());

				closeTable();
			} catch (final Exception e) {
				printError(e, "unknown error");
			}
		}

		private boolean exists(final String id) {
			while (true) {
				try {
					final Get get = new Get(Bytes.toBytes("sf:" + id));
					get.addColumn(meta_family, meta_qualifier);
					return table.exists(get);
				} catch (final IOException e) {
					printError(e, "hbase read error: " + id);
					getTable();
				}
			}
		}

		private void storePut(final Put put) {
			while (true) {
				try {
					table.put(put);
					break;
				} catch (IllegalArgumentException e) {
					// probably tried inserting an empty put, ignore it
					break;
				} catch (final IOException e) {
					printError(e, "error writing table row");
					getTable();
				}
			}
		}
	}

	private static void printError(final Exception e, final String message) {
		System.err.println("ERR: " + message);
		if (debug)
			e.printStackTrace();
		else
			System.err.println(e.getMessage());
	}
}
