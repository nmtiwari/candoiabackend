package main;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

import boa.types.Toplevel.Project;

public class DatasetGenerator {
	private ArrayList<String>Repos;
	private ArrayList<Integer>kindRepos;
    private static SequenceFile.Writer projectWriter, astWriter;
    private static Configuration conf = null;
    private static FileSystem fileSystem = null;
    private String path;
    
    
	public DatasetGenerator(String path) throws IOException {
		conf = new Configuration();
        fileSystem = FileSystem.get(conf);
        this.path=path;
        Repos=new ArrayList<String>();
        kindRepos=new ArrayList<Integer>();
        openWriters(this.path);
	}

	public static void main(String[] args) {
		try {
			DatasetGenerator gen=new DatasetGenerator(args[0]);
			gen.generate();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	
	
	private static void openWriters(String repositoryPath) {
        while (true) {
            try {
                projectWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/.CandoiaDataSet/projects-small.seq"), Text.class, BytesWritable.class);
                astWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/.CandoiaDataSet/ast"), Text.class, BytesWritable.class);
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }

    private static void closeWriters() {
        while (true) {
            try {
                projectWriter.close();
                astWriter.close();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }
    
    private static void deleteAstFile(String[] args) {
        try{
            File file = new File(args[1]+"/.CandoiaDataSet/ast");
            file.delete();

        }catch(Exception e){

            e.printStackTrace();

        }
    }

    private void findReposAndGenerateData(String path){
    	File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;
        int flag=0;
        for ( File f : list ) {
            if ( f.isDirectory() ) {
                if (f.getName().equals(".git")) {
                	System.out.println("Found  project:"+f.getParent());
                	Repos.add(path);
                	kindRepos.add(1);
                    SeqGenLocalGIT.generate(path, projectWriter, astWriter);
                }
                if (f.getName().equalsIgnoreCase("db") || f.getName().equalsIgnoreCase("hooks") ||f.getName().equalsIgnoreCase("locks")||f.getName().equalsIgnoreCase("conf")) {
                    flag++;
                    if(flag>=4) {
                    	System.out.println("Found  project:"+f.getParent());
                    	Repos.add(path);
                    	kindRepos.add(0);
                        SeqGenLocalSVN.generate(path, projectWriter, astWriter);
                    }
                }
                else{
                	findReposAndGenerateData(f.getAbsolutePath());
            }
        }

    }
   }
    
    
    private void findReposAndGenerateData(Project.Builder projBuilder,String path,SequenceFile.Writer AstWriter){
    	File root = new File( path );
        File[] list = root.listFiles();

        if (list == null) return;
        int flag=0;
        for ( File f : list ) {
            if ( f.isDirectory() ) {
                if (f.getName().equals(".git")) {
                	System.out.println("Found  project:"+f.getParent());
                	Repos.add(path);
                	kindRepos.add(1);
                    SeqGenLocalGIT.generate(projBuilder,path, AstWriter);
                }
                if (f.getName().equalsIgnoreCase("db") || f.getName().equalsIgnoreCase("hooks") ||f.getName().equalsIgnoreCase("locks")||f.getName().equalsIgnoreCase("conf")) {
                    flag++;
                    if(flag>=4) {
                    	System.out.println("Found  project:"+f.getParent());
                    	Repos.add(path);
                    	kindRepos.add(0);
                        SeqGenLocalSVN.generate(path, projectWriter, AstWriter);
                    }
                }
                else{
                	findReposAndGenerateData(f.getAbsolutePath());
            }
        }
    }
   }
    
    
    
    private void generateMap(String path) throws Exception{
    	String[] pathAST={path,path};
    	MapFileGen.main(pathAST);
    }
    
    public boolean generate(){
    	openWriters(path);
    	findReposAndGenerateData(path);
    	closeWriters();
    	try{
    		generateMap(path+"/.CandoiaDataSet");
    	}catch(Exception ex){
    		ex.printStackTrace();
    	}
    	
    	if(Repos.size()>0)
    		return true;
    	else
    		return false;
    }
    
    public boolean generate(Project.Builder projBuilder,SequenceFile.Writer astWriter){
    	findReposAndGenerateData(projBuilder,path,astWriter);
    	return true;

    }
    
    
}
