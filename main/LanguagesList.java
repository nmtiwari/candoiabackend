package main;

import java.util.HashMap;

public class LanguagesList {
	private static HashMap<String,String> languages=new HashMap<String,String>();
	public LanguagesList() {
		languages.put("java", "java");
		languages.put("c", "c");
		languages.put("cpp", "c++");
		languages.put("py", "python");
		languages.put("js", "java script");
		languages.put(".net", ".net");
		languages.put("php", "php");
		languages.put("aspx", "asp .net");
		languages.put("jsp", "java servlets");
		languages.put("json", "json");
		languages.put("html", "html");
		languages.put("pl", "perl");
		languages.put("sh", "unix shell");
		languages.put("pas", "Delphy");
		languages.put("sql", "mysql");

	}

	public String hasLang(String ext){
		return languages.get(ext);
	}
}
