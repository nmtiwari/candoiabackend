package main;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.BytesWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;

public class GenerateLocalProjectDataset {
	 private static ArrayList<String>listOfProjects;
	 private static SequenceFile.Writer projectWriter, astWriter;
	    private static Configuration conf = null;
	    private static FileSystem fileSystem = null;
	    
		private static FilenameFilter filter = new FilenameFilter() {
			  @Override
			  public boolean accept(File current, String name) {
			    return new File(current, name).isDirectory();
			  }
			};
	    
	 
	public static void main(String[] args) {
		System.out.println(args[0]);
		 listOfProjects=new ArrayList<String>();
		 conf = new Configuration();
	     try {
			fileSystem = FileSystem.get(conf);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	     openWriters(args[1]);
		searchAndGenerateNestedProjects(args[0]);
		for(String s:listOfProjects){
			if(s.endsWith("extraAddedGit")){
				System.out.println("Writing:"+s.substring(0, s.length()-13));
				SeqGenLocalGIT.generate(s.substring(0, s.length()-13),projectWriter, astWriter);
			}
			else{
				
			}
		}
			
	}

	
    public static void openWriters(String repositoryPath) {
        while (true) {
            try {
                projectWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/projects-small.seq"), Text.class, BytesWritable.class);
                astWriter = SequenceFile.createWriter(fileSystem, conf, new Path(repositoryPath+"/ast"), Text.class, BytesWritable.class);
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }
	
	private static boolean isGitRepo(String rootPath){
		File directory = new File(rootPath);
		boolean isGit=false;
		String[] fList=null;
		if(directory.isDirectory()){
			fList= directory.list(filter);
    		for(String s:fList){
    			if(s.endsWith(".git"))
    				isGit=true;
    			if(isGit)
    				return true;
    		}
		}
		
    		return false;
	}
	
	private static boolean isSvnRepo(String rootPath){
		File directory = new File(rootPath);
		String[] fList =null;
		
		boolean isSvn=false;
		boolean dbDirAvailable=false;
		boolean confDirAvailable=false;
//		boolean formatDirAvailable=false;
		boolean hooksDirAvailable=false;
		boolean lockDirAvailable=false;
		
		if(directory.isDirectory()){
			fList = directory.list(filter);
			  for (String file : fList) {
	    		  if(isSvn || file.endsWith(".svn"))
					return true;
	    		  if (file.endsWith("db")) {
	    			  dbDirAvailable=true;
	    	        }
	    		  else if(file.endsWith("conf")){
	    			  confDirAvailable=true;
	    		  }
	    		  else if(file.endsWith("hooks")){
	    			  hooksDirAvailable=true;
	    		  }
	    		  else if(file.endsWith("locks")){
	    			  lockDirAvailable=true;
	    		  }
	      	    }
		}


		  isSvn=(dbDirAvailable && confDirAvailable  && hooksDirAvailable && lockDirAvailable);
		  return isSvn;
	}
	
	
    private static int searchAndGenerateNestedProjects(String rootPath){
    	File directory=new File(rootPath);
    	if(directory.isDirectory()){
    		directory = new File(rootPath);
       	 String[] fList = directory.list(filter);
       	  for (String file : fList) {
       		  if (isGitRepo(rootPath+"/"+file)) {
       	            if(!listOfProjects.contains(rootPath)){
       	            	rootPath=rootPath+"extraAddedGit";
       	            	listOfProjects.add(rootPath);
       	            }
       	        }
       		  else if(isSvnRepo(rootPath+"/"+file)){
       			  if(!listOfProjects.contains(rootPath)){
       				  rootPath=rootPath+"extraAddedSvn"; 
     	            	listOfProjects.add(rootPath); 
       			  }
       		  }
       		  else{
       			  searchAndGenerateNestedProjects(rootPath+"/"+file);
       		  }
       	    }
    	}
    	return listOfProjects.size();
    }
    

    public static void cleanup(String[] args) {
        try{
            File file = new File(args[1]+"/data");
            file.delete();
            
            
            File Indexfile = new File(args[1]+"/index");
            Indexfile.delete();
            
            File Projectsfile = new File(args[1]+"/projects-small.seq");
            Projectsfile.delete();

        }catch(Exception e){

            e.printStackTrace();

        }
    }
    

    public static void closeWriters() {
        while (true) {
            try {
                projectWriter.close();
                astWriter.close();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
                break;
            } catch (Throwable t) {
                t.printStackTrace();
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {}
            }
        }
    }
    
}
